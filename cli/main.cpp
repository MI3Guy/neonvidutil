#include "core/NeonConfig.h"
#include "core/NeonContext.h"
#include "core/EncodePath.h"
#include "core/lang.h"

#include <tclap/CmdLine.h>
#include <boost/format.hpp>

#include <iostream>
#include <string>

using namespace Neon::Core;

int main(int argc, char** argv)
{
	
	//std::cout << boost::format("NeonVidUtil %1%.%2%") % Neon::Core::VersionMajor % Neon::Core::VersionMinor << std::endl;

	Neon::Core::NeonContext ctx;

	std::string inputFile;
	std::vector<std::string> outputFiles;

	try
	{
		TCLAP::CmdLine cmd("", ' ', (boost::format("%1%.%2%") % Neon::Core::VersionMajor % Neon::Core::VersionMinor).str());


		// TODO: Add unload plugin options.



		TCLAP::UnlabeledValueArg<std::string> inputFileArg("input", "Input Filename"_gt, true, "", "The input file that will be converted."_gt, cmd);
		TCLAP::UnlabeledMultiArg<std::string> outputFileArgs("output", "Output Filename"_gt, false, "The output file that will be created. Can support a stream index number (streamIndex:filename)."_gt, cmd);

		cmd.parse(argc, argv);

		inputFile = inputFileArg.getValue();
		outputFiles = outputFileArgs.getValue();
	}
	catch(TCLAP::ArgException& e)
	{
		std::cerr << boost::format("Error Parsing Arguments: %1% for argument %2%") % e.error() % e.argId() << std::endl;
		return 1;
	}

	auto inputFileInfo = ctx.IdentifyInputFile(inputFile);

	if(inputFileInfo.size() == 0)
	{
		std::cout << "Could not identify input file."_gt << std::endl;
		return 1;
	}


	if(inputFileInfo.size() != 1)
	{
		printf("Input file %s contains multiple files.", inputFile.c_str());
		std::cout << std::endl;
	}

	for(auto& fileInfo : inputFileInfo)
	{
		std::cout << "Read tracks from input file "_gt << std::get<0>(fileInfo) << std::endl;
		std::get<1>(fileInfo).WriteInfo(stdout);
	}

	// Only a single input file is supported.
	if(inputFileInfo.size() != 1)
	{
		return 1;
	}

	// No output files. Input file info was already reported so just exit.
	if(outputFiles.size() == 0)
	{
		return 1;
	}

	if(outputFiles.size() > 1)
	{
		std::cerr << "Multiple output files not yet supported.\n";
		return 1;
	}

	auto outputFileInfo = ctx.IdentifyOutputFile(outputFiles[0]);
	if(!outputFileInfo)
	{
		std::cerr << "Could not identify output format.\n";
		return 1;
	}

	EncodePath path(ctx, std::get<1>(inputFileInfo[0]), *outputFileInfo);

	std::cout << "Found encode path\n";

	for(auto& step : path.GetSteps())
	{
		std::cout << step->GetName() << "\n";
	}

	path.RunConversion(inputFile, outputFiles[0]);

	return 0;
}

