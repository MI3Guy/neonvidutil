require 'rake/clean'
require 'rake/loaders/makefile'
require 'fileutils'

if !File.exist? 'build_config.rb'
	FileUtils.cp 'build_config.rb.default', 'build_config.rb'
end

require_relative 'build_config.rb'

CC = 'gcc'
CPP = 'g++'
CORE_LIBRARIES = [ 'pthread' ]
CC_OPTIONS =
[
	'-O0', '-g', '-Wall', '-Wno-unused-result', '-Wconversion', '-fPIC',
	'-MMD', '-I.'
] + USER_CC_OPTIONS

CPP_OPTIONS = CC_OPTIONS + 
[
	'--std=c++11', '-Werror'
]



BUILD_SETTINGS_FILE = 'build_settings.rb'

def try_compile(libs, code)
	begin
		IO.popen [
			CPP,
				*CPP_OPTIONS,
				'-x', 'c++', '-',
				'-o', 'test',
				*(CORE_LIBRARIES + libs).map { |lib| "-l#{lib}" }
			],
			"r+" do |io|
			io.puts code
			io.close
			
			$?.to_i == 0 
		end
		
		['test', 'test.exe', 'test.d', 'test.mk'].each do |file| File.delete file if File.exist? file end
	rescue
		false
	end
end

class PluginInfo
	def initialize(enabled: false, dependencies: [], libraries: [])
		@enabled = enabled
		@dependencies = dependencies
		@libraries = libraries
	end
	
	attr_accessor :enabled, :dependencies, :libraries
end

def generate_build_settings(filename)
	puts "Detecting enabled plugins"

	pluginConfigs = Hash[ Dir['plugins/*'].keep_if{ |dir| File.directory? dir }.map{ |dir| [File.basename(dir), dir + "/config.rb"] }.keep_if { |name, config| File.exists? config } ]
	plugins = Hash[ pluginConfigs.map { |name, config| [name, eval(File.read(config), nil, config)] } ]
	
	plugins.each do |name, plugin|
		puts "Plugin #{name} is #{if plugin.enabled then "enabled" else "disabled" end}"
	end
	 
	enabledPlugins = plugins.keep_if { |name, plugin| plugin.enabled }
	
	enabledPlugins.each do |name, plugin|
		plugin.dependencies.each do |dep|
			raise "Enabled plugin #{name} requires disabled/mising plugin #{dep}" if !enabledPlugins.has_key? dep
		end
	end
	
	enabledPluginNames = enabledPlugins.keys
	pluginLibs = enabledPlugins.map { |name, plugin| plugin.libraries }.flatten

	File.open filename, "w" do |f|
		f.puts "ENABLED_PLUGINS = #{enabledPluginNames.inspect}"
		f.puts "PLUGIN_LIBRARIES = #{pluginLibs.inspect}"
	end
end


if !File.exist? BUILD_SETTINGS_FILE
	generate_build_settings BUILD_SETTINGS_FILE
end

require_relative BUILD_SETTINGS_FILE 

CORE_SOURCE = FileList['core/*.cpp'] + FileList["core/*.c"]
CLI_SOURCE = FileList['cli/*.cpp'] + FileList["cli/*.c"]

PLUGIN_SOURCE = Dir['plugins/*'].
		keep_if{ |dir| File.directory? dir }.
		map{ |dir| [File.basename(dir), FileList["#{dir}/*.cpp"] + FileList["#{dir}/*.c"]] }.
		keep_if{ |name, files| !ENABLED_PLUGINS.nil? && ENABLED_PLUGINS.include?(name) }.
		map{ |name, files| files}.
		flatten

ALL_SOURCE = CORE_SOURCE + CLI_SOURCE + PLUGIN_SOURCE
ALL_CLI_OBJ = (CORE_SOURCE + CLI_SOURCE + PLUGIN_SOURCE).ext('.o')

CLI_EXECUTABLE = 'neonvidutil'

LIBRARIES = (CORE_LIBRARIES + PLUGIN_LIBRARIES + USER_LIBRARIES).uniq


file CLI_EXECUTABLE => ALL_CLI_OBJ do |t|
	sh CPP,
		*CPP_OPTIONS,
		*t.prerequisites,
		'-o', t.name,
		*LIBRARIES.map { |lib| "-l#{lib}" }
end

rule '.o' => ['.cpp'] do |t|
	sh CPP,
		*CPP_OPTIONS,
		t.source,
		'-c', '-o', t.name, '-MF', t.name.ext('.mf')
end

rule '.o' => ['.c'] do |t|
	sh CC,
		*CC_OPTIONS,
		t.source,
		'-c', '-o', t.name, '-MF', t.name.ext('.mf')
end

ALL_SOURCE.each do |src|
	file src.ext('.o') => src
end

FileList['**/*.mf'].each do |dep|
	import dep
end

task :default => CLI_EXECUTABLE
task :all => :default
task :config do
	generate_build_settings BUILD_SETTINGS_FILE
end

CLEAN.include '**/*.o'
CLEAN.include '**/*.mf'
CLEAN.include 'neonvidutil'

