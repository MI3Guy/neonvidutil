/*
 * WAVRIFF.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#ifndef WAVRIFF_H_
#define WAVRIFF_H_

#include "core/BinaryReader.h"
#include "core/BufferCompare.h"

#include <cstdint>

namespace Neon {
namespace Plugin {
namespace WAV {

class WAVRIFF {
public:
	WAVRIFF(Core::BinaryReaderLE& reader);
	WAVRIFF();

	~WAVRIFF();

	char ckID[4];
	std::uint32_t size;
	char WAVEID[4];

	bool IsValid() const
	{
		return Core::BufferEquals(ckID, ChunkIdRiff, sizeof(ckID)) && Core::BufferEquals(WAVEID, ChunkIdWave, sizeof(WAVEID));
	}

	static constexpr char ChunkIdRiff[] = "RIFF";
	static constexpr char ChunkIdWave[] = "WAVE";
};

} /* namespace WAV */
} /* namespace Plugin */
} /* namespace Neon */

#endif /* WAVRIFF_H_ */
