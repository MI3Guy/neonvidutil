/*
 * WAVReader.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "core/BinaryReader.h"

#include <exception>
#include <memory>
#include "plugins/wav/WAVRIFF.h"
#include "plugins/wav/WAVFormatChunk.h"
#include "plugins/wav/WAVDataChunk.h"

namespace Neon {
namespace Plugin {
namespace WAV {

class WAVReader {
public:
	class InvalidWAVFile : public std::exception {};
	class UnsupportedBitDepth : public std::exception {};

	WAVReader(Core::BinaryReaderLE& reader);
	virtual ~WAVReader();

private:
	Core::BinaryReaderLE& reader;

	std::shared_ptr<WAVRIFF> riffHeader;
	std::shared_ptr<WAVFormatChunk> format;
	std::shared_ptr<WAVDataChunk> data;

public:
	const WAVRIFF& GetRiffHeader() const;
	const WAVFormatChunk& GetFormat() const;
	const WAVDataChunk& GetData() const;

	std::size_t ReadSamples(std::int32_t* buffer, std::size_t size);
};

} /* namespace WAV */
} /* namespace Plugin */
} /* namespace Neon */

