/*
 * WAVFormatHandler.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#pragma once

#include "core/FormatHandler.h"

namespace Neon {
namespace Plugin {
namespace WAV {

class WAVFormatHandler : public Core::FormatHandler {
public:
	WAVFormatHandler();
	virtual ~WAVFormatHandler();

	virtual std::string GetName() const override;
	virtual std::vector<std::tuple<std::string, Core::FormatType>> IdentifyInput(const std::string& path) const override;

	static Core::FormatType WavFormat(std::string info = std::string());
};

} /* namespace WAV */
} /* namespace Plugin */
} /* namespace Neon */
