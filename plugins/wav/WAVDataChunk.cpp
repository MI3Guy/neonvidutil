/*
 * WAVDataChunk.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "WAVDataChunk.h"

#include <limits>

namespace Neon {
namespace Plugin {
namespace WAV {

WAVDataChunk::WAVDataChunk(Core::BinaryReaderLE& reader)
{
	cksize = reader.ReadUInt32();

	useStartPos = reader.GetInputStream().CanSeek() &&
		reader.GetInputStream().Length() <= std::numeric_limits<std::uint32_t>::max() &&
		cksize != 0;

	if(useStartPos)
	{
		startPos = reader.GetInputStream().Position();
	}
}

WAVDataChunk::WAVDataChunk()
	: cksize(0), useStartPos(false)
{

}

WAVDataChunk::~WAVDataChunk() {
}

constexpr char WAVDataChunk::ChunkId[];

} /* namespace WAV */
} /* namespace Plugin */
} /* namespace Neon */
