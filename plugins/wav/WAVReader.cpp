/*
 * WAVReader.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */
#include <iostream>
#include "WAVReader.h"

namespace Neon {
namespace Plugin {
namespace WAV {

WAVReader::WAVReader(Core::BinaryReaderLE& reader_)
	: reader(reader_)
{
	riffHeader = std::shared_ptr<WAVRIFF>(new WAVRIFF(reader));

	if(!riffHeader->IsValid())
	{
		throw InvalidWAVFile();
	}

	while(true)
	{
		char ckID[4];
		reader.ReadBytes(ckID, sizeof(ckID));

		if(Core::BufferEquals(ckID, WAVFormatChunk::ChunkId, sizeof(ckID)))
		{
			if(format)
			{
				throw InvalidWAVFile();
			}

			format = std::shared_ptr<WAVFormatChunk>(new WAVFormatChunk(reader));

			if(format->wFormatTag != WAVFormatChunk::FormatTag::PCM &&
				(format->wFormatTag != WAVFormatChunk::FormatTag::EXTENSIBLE ||
						!Core::BufferEquals(format->SubFormat, WAVFormatChunk::FormatSubtypePCM, sizeof(format->SubFormat)) )
			)
			{
				throw InvalidWAVFile();
			}
		}
		else if(Core::BufferEquals(ckID, WAVDataChunk::ChunkId, sizeof(ckID)))
		{
			if(!format)
			{
				throw InvalidWAVFile();
			}

			data = std::shared_ptr<WAVDataChunk>(new WAVDataChunk(reader));
			break;
		}
		else
		{
			std::uint32_t size = reader.ReadUInt32();
			reader.SkipBytes(size);
		}

	}

}

WAVReader::~WAVReader() {
	// TODO Auto-generated destructor stub
}

const WAVRIFF& WAVReader::GetRiffHeader() const
{
	return *riffHeader;
}

const WAVFormatChunk& WAVReader::GetFormat() const
{
	return *format;
}

const WAVDataChunk& WAVReader::GetData() const
{
	return *data;
}

std::size_t WAVReader::ReadSamples(std::int32_t* buffer, std::size_t size)
{
	std::size_t numRead;

	std::uint16_t numBytes = format->wBitsPerSample / 8;

	if(numBytes == 0 || numBytes > 4)
	{
		throw UnsupportedBitDepth();
	}

	for(numRead = 0; numRead < size; ++numRead)
	{
		if(data->useStartPos && reader.GetInputStream().Position() > data->startPos + data->cksize)
		{
			break;
		}

		std::uint8_t intBuffer[4];
		try
		{
			reader.ReadBytes(intBuffer, numBytes);
		}
		catch(Core::BinaryReaderLE::EndOfFileException&)
		{
			break;
		}

		switch(numBytes)
		{
			case 1:
				buffer[numRead] = intBuffer[0];
				break;

			case 2:
				buffer[numRead] = static_cast<std::int32_t>(static_cast<std::int8_t>(intBuffer[1])) << 8 | intBuffer[0];
				break;

			case 3:
				buffer[numRead] = static_cast<std::int32_t>(static_cast<std::int8_t>(intBuffer[2])) << 16 | intBuffer[1] << 8 | intBuffer[0];
				break;

			case 4:
				buffer[numRead] = static_cast<std::int32_t>(static_cast<std::int8_t>(intBuffer[3])) << 24 | intBuffer[2] << 16 | intBuffer[1] << 8 | intBuffer[0];
				break;
		}
	}

	return numRead;
}

} /* namespace WAV */
} /* namespace Plugin */
} /* namespace Neon */
