/*
 * WAVDataChunk.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#pragma once

#include "core/BinaryReader.h"
#include "core/InputStream.h"
#include "plugins/wav/WAVFormatChunk.h"


#include <cstdint>
#include <cstdio>
#include <memory>

namespace Neon {
namespace Plugin {
namespace WAV {

class WAVDataChunk {
public:
	WAVDataChunk(Core::BinaryReaderLE& reader);
	WAVDataChunk();
	virtual ~WAVDataChunk();

	static constexpr char ChunkId[] = "data";

	std::uint32_t cksize;

	bool useStartPos;
	Core::InputStream::LengthType startPos;

};

} /* namespace WAV */
} /* namespace Plugin */
} /* namespace Neon */

