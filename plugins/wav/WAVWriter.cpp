/*
 * WAVWriter.cpp
 *
 *  Created on: Mar 29, 2014
 *      Author: john
 */

#include "WAVWriter.h"

namespace Neon {
namespace Plugin {
namespace WAV {

WAVWriter::WAVWriter(std::uint16_t channels, std::uint32_t samplesPerSec, std::uint16_t bytesPerSample, std::uint16_t validBitsPerSample, WAVFormatChunk::Speaker speakerLayout)
	: WAVWriter(WAVRIFF(), WAVFormatChunk(channels, samplesPerSec, bytesPerSample, validBitsPerSample, speakerLayout), WAVDataChunk())
{
}

WAVWriter::WAVWriter(const WAVRIFF& riffHeader_, const WAVFormatChunk& formatChunk, const WAVDataChunk& dataChunk)
	: riffHeader(riffHeader_), format(formatChunk), data(dataChunk)
{
}

WAVWriter::~WAVWriter() {
}

void WAVWriter::WriteHeaders(Core::BinaryWriterLE& writer)
{
	auto& stream = writer.GetOutputStream();

	// RIFF
	stream.Write(riffHeader.ckID, sizeof(riffHeader.ckID));
	writer.WriteUInt32(riffHeader.size);
	stream.Write(riffHeader.WAVEID, sizeof(riffHeader.WAVEID));

	// Format
	stream.Write(WAVFormatChunk::ChunkId, 4);

	if(format.wFormatTag == WAVFormatChunk::FormatTag::EXTENSIBLE)
	{
		if(format.cbSize > 0)
		{
			writer.WriteUInt32(WAVFormatChunk::expectedSizeFull);
		}
		else
		{
			writer.WriteUInt32(WAVFormatChunk::expectedSizeMore);
		}
	}
	else
	{
		writer.WriteUInt32(WAVFormatChunk::expectedSizeSimple);
	}

	writer.WriteUInt16(static_cast<std::uint16_t>(format.wFormatTag));
	writer.WriteUInt16(format.nChannels);
	writer.WriteUInt32(format.nSamplesPerSec);
	writer.WriteUInt32(format.nChannels * format.nSamplesPerSec * (format.wBitsPerSample / 8)); // nAvgBytesPerSec
	writer.WriteUInt16(static_cast<std::uint16_t>(format.nChannels * (format.wBitsPerSample / 8)));
	writer.WriteUInt16(format.wBitsPerSample);

	if(format.wFormatTag == WAVFormatChunk::FormatTag::EXTENSIBLE)
	{
		if(format.cbSize > 0)
		{
			writer.WriteUInt16(WAVFormatChunk::expectedSizeFull - WAVFormatChunk::expectedSizeMore);
			writer.WriteUInt16(format.wValidBitsPerSample);
			writer.WriteUInt32(static_cast<std::uint32_t>(format.dwChannelMask));
			stream.Write(format.SubFormat, sizeof(format.SubFormat));
		}
		else
		{
			writer.WriteUInt16(0);
		}
	}

	// Data
	stream.Write(data.ChunkId, 4);
	writer.WriteUInt32(data.useStartPos ? data.cksize : 0);

}

void WAVWriter::WriteSamples(Core::BinaryWriterLE& writer, const std::int32_t* buffer, std::size_t size)
{
	auto& stream = writer.GetOutputStream();

	std::uint16_t numBytes = format.wBitsPerSample / 8;

	if(numBytes == 0 || numBytes > 4)
	{
		throw UnsupportedBitDepth();
	}

	for(std::size_t i = 0; i < size; ++i)
	{
		std::uint8_t intBuffer[4];
		std::int32_t currentValue = buffer[i];

		for(std::uint16_t currentByte = 0; currentByte < numBytes; ++currentByte, currentValue >>= 8)
		{
			intBuffer[currentByte] = static_cast<std::uint8_t>(currentValue & 0xFF);
		}

		stream.Write(intBuffer, numBytes);
	}
}

} /* namespace WAV */
} /* namespace Plugin */
} /* namespace Neon */
