/*
 * WAVFormatHandler.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "WAVFormatHandler.h"

#include "core/FileInputStream.h"
#include "plugins/wav/WAVReader.h"
#include "core/lang.h"

#include <boost/format.hpp>

using namespace Neon::Core;

namespace Neon {
namespace Plugin {
namespace WAV {

WAVFormatHandler::WAVFormatHandler() {
	outputFormats["wav"] = WavFormat();

}

WAVFormatHandler::~WAVFormatHandler() {
	// TODO Auto-generated destructor stub
}

std::string WAVFormatHandler::GetName() const
{
	return "WAV";
}

std::vector<std::tuple<std::string, Core::FormatType>> WAVFormatHandler::IdentifyInput(const std::string& path) const
{
	try
	{
		Core::FileInputStream stream(path);
		Core::BinaryReaderLE reader(stream);

		WAVReader wavFile(reader);

		const WAVFormatChunk& format = wavFile.GetFormat();
		auto bitDepth = format.wBitsPerSample;
		auto sampleRate = format.nSamplesPerSec;
		auto channels = format.nChannels;

		return
		{
			std::make_tuple(path, WavFormat((boost::format("%1% Channel(s), %2%-bit, %3%Hz"_gt) % channels % bitDepth % sampleRate).str()))
		};
	}
	catch(...)
	{
		return std::vector<std::tuple<std::string, Core::FormatType>>();
	}
}

Core::FormatType WAVFormatHandler::WavFormat(std::string info)
{
	return Core::FormatType(
		Core::FormatType::Container::Wave,
		std::vector<Core::TrackInfo> { Core::TrackInfo(Core::TrackInfo::Codec::PCM, info) }
	);
}

Core::FormatHandlerFactory<WAVFormatHandler> factory;

} /* namespace WAV */
} /* namespace Plugin */
} /* namespace Neon */
