/*
 * WAVRIFF.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "WAVRIFF.h"

namespace Neon {
namespace Plugin {
namespace WAV {

constexpr char WAVRIFF::ChunkIdRiff[];
constexpr char WAVRIFF::ChunkIdWave[];

WAVRIFF::WAVRIFF(Core::BinaryReaderLE& reader)
{
	reader.ReadBytes(ckID, sizeof(ckID));
	size = reader.ReadUInt32();
	reader.ReadBytes(WAVEID, sizeof(WAVEID));
}


WAVRIFF::WAVRIFF() {
	memcpy(ckID, ChunkIdRiff, sizeof(ckID));
	size = 0;
	memcpy(WAVEID, ChunkIdWave, sizeof(WAVEID));
}

WAVRIFF::~WAVRIFF() {
}



} /* namespace WAV */
} /* namespace Plugin */
} /* namespace Neon */
