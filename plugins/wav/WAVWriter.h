/*
 * WAVWriter.h
 *
 *  Created on: Mar 29, 2014
 *      Author: john
 */

#pragma once

#include "plugins/wav/WAVRIFF.h"
#include "plugins/wav/WAVFormatChunk.h"
#include "plugins/wav/WAVDataChunk.h"
#include "core/BinaryWriter.h"

#include <cstring>
#include <cstdint>

namespace Neon {
namespace Plugin {
namespace WAV {

class WAVWriter {
public:
	class UnsupportedBitDepth : public std::exception {};

	WAVWriter(std::uint16_t channels, std::uint32_t samplesPerSec, std::uint16_t bytesPerSample, std::uint16_t validBitsPerSample, WAVFormatChunk::Speaker speakerLayout);
	WAVWriter(const WAVRIFF& riffHeader_, const WAVFormatChunk& formatChunk, const WAVDataChunk& dataChunk);
	virtual ~WAVWriter();

private:
	WAVRIFF riffHeader;
	WAVFormatChunk format;
	WAVDataChunk data;

public:
	void WriteHeaders(Core::BinaryWriterLE& writer);
	void WriteSamples(Core::BinaryWriterLE& writer, const std::int32_t* buffer, std::size_t size);
};

} /* namespace WAV */
} /* namespace Plugin */
} /* namespace Neon */

