/*
 * WAVFormatChunk.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#pragma once

#include "core/BinaryReader.h"
#include <cinttypes>
#include <exception>

namespace Neon {
namespace Plugin {
namespace WAV {

class WAVFormatChunk {
public:
	class TooManyBytesRead : std::exception {};

	static constexpr char ChunkId[] = "fmt ";

	static constexpr std::uint32_t expectedSizeSimple = 2 + 2 + 4 + 4 + 2 + 2;
	static constexpr std::uint32_t expectedSizeMore = expectedSizeSimple + 2;
	static constexpr std::uint32_t expectedSizeFull = expectedSizeMore + 2 + 4 + 16;

	enum class FormatTag : std::uint16_t
	{
		PCM			= 0x0001,
		IEEE_FLOAT	= 0x0003,
		ALAW		= 0x0006,
		MULAW		= 0x0007,
		EXTENSIBLE	= 0xFFFE
	};

	enum class Speaker : std::uint32_t
	{
		None 				=	0x0,
		FrontLeft			=	0x1,
		FrontRight			=	0x2,
		FrontCenter			=	0x4,
		LowFrequency		=	0x8,
		BackLeft			=	0x10,
		BackRight			=	0x20,
		FrontLeftOfCenter	=	0x40,
		FrontRightOfCenter	=	0x80,
		BackCenter			=	0x100,
		SideLeft			=	0x200,
		SideRight			=	0x400,
		TopCenter			=	0x800,
		TopFrontLeft		=	0x1000,
		TopFrontCenter		=	0x2000,
		TopFrontRight		=	0x4000,
		TopBackLeft			=	0x8000,
		TopBackCenter		=	0x10000,
		TopBackRight		=	0x20000,

		Mono				=	FrontCenter,
		Stereo				=	FrontLeft | FrontRight,
		Quad				=	FrontLeft | FrontRight | BackLeft | BackRight,
		Surround			=	FrontLeft | FrontRight | FrontCenter | BackCenter,
		FivePointOne		=	FrontLeft | FrontRight | FrontCenter | LowFrequency | BackLeft | BackRight,
		SevenPointOne		=	FrontLeft | FrontRight | FrontCenter | LowFrequency | BackLeft | BackRight | FrontLeftOfCenter | FrontRightOfCenter,
		SevenPointOneReal	=	FrontLeft | FrontRight | FrontCenter | LowFrequency | BackLeft | BackRight | SideLeft | SideRight,

		FLACThreeChannel = FrontLeft | FrontRight | FrontCenter,
		FLACFiveChannel = FrontLeft | FrontRight | FrontCenter | BackLeft | BackRight,
	};

	static constexpr unsigned char FormatSubtypePCM[] = { 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71 };

	WAVFormatChunk(Core::BinaryReaderLE& reader);
	WAVFormatChunk(std::uint16_t channels, std::uint32_t samplesPerSec, std::uint16_t bytesPerSample, std::uint16_t validBitsPerSample, Speaker speakerLayout);
	virtual ~WAVFormatChunk();


	std::uint32_t cksize;
	FormatTag wFormatTag;
	std::uint16_t nChannels;
	std::uint32_t nSamplesPerSec;
	std::uint32_t nAvgBytesPerSec;
	std::uint16_t nBlockAlign;
	std::uint16_t wBitsPerSample;
	std::uint16_t cbSize;
	std::uint16_t wValidBitsPerSample;
	Speaker dwChannelMask;
	unsigned char SubFormat[sizeof(FormatSubtypePCM)];

};

} /* namespace WAV */
} /* namespace Plugin */
} /* namespace Neon */

