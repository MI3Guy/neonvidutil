/*
 * WAVFormatChunk.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "WAVFormatChunk.h"

namespace Neon {
namespace Plugin {
namespace WAV {

WAVFormatChunk::WAVFormatChunk(Core::BinaryReaderLE& reader) {
	cksize = reader.ReadUInt32();

	uint32_t numRead = expectedSizeSimple;

	wFormatTag = static_cast<FormatTag>(reader.ReadUInt16());
	nChannels = reader.ReadUInt16();
	nSamplesPerSec = reader.ReadUInt32();
	nAvgBytesPerSec = reader.ReadUInt32();
	nBlockAlign = reader.ReadUInt16();
	wBitsPerSample = reader.ReadUInt16();
	if(wFormatTag == FormatTag::EXTENSIBLE)
	{
		cbSize = reader.ReadUInt16();
		numRead = expectedSizeMore;
		if(cbSize > 0)
		{
			wValidBitsPerSample = reader.ReadUInt16();
			dwChannelMask = static_cast<Speaker>(reader.ReadUInt32());
			reader.ReadBytes(SubFormat, sizeof(SubFormat));
			numRead = expectedSizeFull;
		}
		else
		{
			wValidBitsPerSample = wBitsPerSample;
			dwChannelMask = Speaker::None;
			memcpy(SubFormat, FormatSubtypePCM, sizeof(SubFormat));
		}
	}
	else
	{
		cbSize = 0;

		wValidBitsPerSample = wBitsPerSample;
		dwChannelMask = Speaker::None;
		if(wFormatTag == FormatTag::PCM)
		{
			memcpy(SubFormat, FormatSubtypePCM, sizeof(SubFormat));
		}
		else
		{
			memset(SubFormat, 0, sizeof(SubFormat));
		}
	}

	if(cksize > numRead)
	{
		std::uint32_t skipBytes = cksize - numRead;
		reader.SkipBytes(skipBytes);
	}
	else if(numRead > cksize)
	{
		throw TooManyBytesRead();
	}
}

WAVFormatChunk::WAVFormatChunk(std::uint16_t channels, std::uint32_t samplesPerSec, std::uint16_t bytesPerSample, std::uint16_t validBitsPerSample, Speaker speakerLayout)
	: cksize(expectedSizeFull),
	  wFormatTag(FormatTag::EXTENSIBLE),
	  nChannels(channels),
	  nSamplesPerSec(samplesPerSec),
	  nAvgBytesPerSec(static_cast<std::uint32_t>(channels * samplesPerSec * bytesPerSample)),
	  nBlockAlign(static_cast<std::uint16_t>(channels * bytesPerSample)),
	  wBitsPerSample(static_cast<std::uint16_t>(bytesPerSample * 8)),
	  cbSize(expectedSizeFull - expectedSizeMore),
	  wValidBitsPerSample(validBitsPerSample),
	  dwChannelMask(speakerLayout)
{
	memcpy(SubFormat, FormatSubtypePCM, sizeof(SubFormat));
}

WAVFormatChunk::~WAVFormatChunk()
{
}

constexpr char WAVFormatChunk::ChunkId[];
constexpr unsigned char WAVFormatChunk::FormatSubtypePCM[];

} /* namespace WAV */
} /* namespace Plugin */
} /* namespace Neon */
