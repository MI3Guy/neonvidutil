/*
 * WavPackEncoder.cpp
 *
 *  Created on: Mar 30, 2014
 *      Author: john
 */

#include "plugins/wavpack/WavPackEncoder.h"

#include "plugins/wav/WAVReader.h"

#include <wavpack/wavpack.h>
#include <vector>
#include <limits>

namespace Neon {
namespace Plugin {
namespace WavPack {

WavPackEncoder::WavPackEncoder()
{
}

WavPackEncoder::~WavPackEncoder()
{
}

std::string WavPackEncoder::GetName() const
{
	return "WAV => WavPack";
}

void WavPackEncoder::ConvertData(Core::IOStreamPair streams)
{
	Core::BinaryReaderLE reader(*streams.Input);
	WAV::WAVReader wavReader(reader);

	struct WriteId
	{
		std::shared_ptr<Core::OutputStream> stream;
		std::vector<std::uint8_t> firstBlock;
	} writeId = { streams.Output };

	auto blockOutput = [](void* id, void* data, int32_t bcount)
	{
		try
		{
			if(bcount >= 0)
			{
				auto writeId = static_cast<WriteId*>(id);
				writeId->stream->Write(data, static_cast<uint32_t>(bcount));
				if(writeId->firstBlock.size() == 0)
				{
					auto byteData = static_cast<std::uint8_t*>(data);
					writeId->firstBlock = std::vector<std::uint8_t>(byteData, byteData + bcount);
				}
			}
			return 1;
		}
		catch(...)
		{
			return 0;
		}
	};

	WavpackContext* ctx = WavpackOpenFileOutput(blockOutput, &writeId, nullptr);

	if(ctx == nullptr)
	{
		throw ConversionError("Error creating wavpack output context.");
	}

	auto format = wavReader.GetFormat();

	if(format.nSamplesPerSec > static_cast<std::uint32_t>(std::numeric_limits<std::int32_t>::max()))
	{
		throw ConversionError("Input sample rate is too high.");
	}

	WavpackConfig config = {};
	config.bytes_per_sample = format.wBitsPerSample / 8;
	config.bits_per_sample = format.wValidBitsPerSample;
	config.channel_mask = static_cast<int32_t>(format.dwChannelMask);
	config.num_channels = format.nChannels;
	config.sample_rate = format.nSamplesPerSec;
	config.flags = CONFIG_VERY_HIGH_FLAG | CONFIG_EXTRA_MODE;
	config.xmode = 6;

	if(!WavpackSetConfiguration(ctx, &config, -1))
	{
		std::string str = WavpackGetErrorMessage(ctx);
		WavpackCloseFile(ctx);
		throw ConversionError(str);
	}

	if(!WavpackPackInit(ctx))
	{
		WavpackCloseFile(ctx);
		throw ConversionError("An error occurred while initializing the encoder.");
	}

	const auto numChannels = wavReader.GetFormat().nChannels;
	const auto sampleRate = wavReader.GetFormat().nSamplesPerSec;
	const std::size_t bufferSize = static_cast<std::size_t>(numChannels) * sampleRate;


	try
	{
		std::vector<int32_t> buffer(bufferSize);

		while(std::size_t numRead = wavReader.ReadSamples(&buffer[0], buffer.size()))
		{
			if(!WavpackPackSamples(ctx, &buffer[0], static_cast<std::uint32_t>(numRead / numChannels)))
			{
				throw ConversionError("An error occurred while encoding.");
			}
		}
	}
	catch(...)
	{
		WavpackCloseFile(ctx);
		throw;
	}


	if(!WavpackFlushSamples(ctx))
	{
		WavpackCloseFile(ctx);
		throw ConversionError("An error occurred while initializing the encoder.");
	}

	try
	{
		if(streams.Output->CanSeek())
		{
			WavpackUpdateNumSamples(ctx, &writeId.firstBlock[0]);
			streams.Output->Seek(0);
			streams.Output->Write(&writeId.firstBlock[0], writeId.firstBlock.size());
		}
	}
	catch(...)
	{
		WavpackCloseFile(ctx);
		throw;
	}

	WavpackCloseFile(ctx);
}


} /* namespace WavPack */
} /* namespace Plugin */
} /* namespace Neon */
