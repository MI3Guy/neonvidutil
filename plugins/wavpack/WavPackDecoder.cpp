/*
 * WavPackDecoder.cpp
 *
 *  Created on: Mar 30, 2014
 *      Author: john
 */

#include "WavPackDecoder.h"

#include "plugins/wav/WAVWriter.h"

#include <wavpack/wavpack.h>
#include <boost/optional.hpp>

namespace Neon {
namespace Plugin {
namespace WavPack {

WavPackDecoder::WavPackDecoder()
{
}

WavPackDecoder::~WavPackDecoder()
{
}

std::string WavPackDecoder::GetName() const
{
	return "WavPack => WAV";
}

void WavPackDecoder::ConvertData(Core::IOStreamPair streams)
{
	struct ReadId
	{
		std::shared_ptr<Core::InputStream> stream;
		bool hasError;
		boost::optional<char> ungottenc;
	} readid = { streams.Input, false };

	WavpackStreamReader streamReader {};
	streamReader.read_bytes = [](void* id, void* data, int32_t bcount)
	{
		auto readid = static_cast<ReadId*>(id);
		try
		{
			if(bcount > 0)
			{
				if(readid->ungottenc)
				{
					*static_cast<char*>(data) = readid->ungottenc.get();
					readid->ungottenc = boost::optional<char>();
					return 1;
				}
				else
				{
					return static_cast<int32_t>(readid->stream->Read(data, static_cast<std::size_t>(bcount)));
				}
			}
			else
			{
				return 0;
			}
		}
		catch(...)
		{
			readid->hasError = true;
			return 0;
		}
	};

	streamReader.get_pos = [](void* id)
	{
		auto readid = static_cast<ReadId*>(id);
		try
		{
			auto pos = readid->stream->Position();
			if(pos > std::numeric_limits<std::uint32_t>::max())
			{
				return static_cast<std::uint32_t>(-1);
			}
			else
			{
				return static_cast<std::uint32_t>(pos);
			}
		}
		catch(...)
		{
			return static_cast<std::uint32_t>(-1);
		}
	};

	streamReader.set_pos_abs = [](void* id, uint32_t pos)
	{
		auto readid = static_cast<ReadId*>(id);
		try
		{
			if(pos > std::numeric_limits<Core::InputStream::LengthType>::max())
			{
				readid->stream->Seek(pos);
				return 0;
			}
			else
			{
				return -1;
			}
		}
		catch(...)
		{
			return -1;
		}
	};

	streamReader.set_pos_rel = [](void* id, int32_t delta, int mode)
	{
		auto readid = static_cast<ReadId*>(id);
		try
		{
			auto seekType = [&]()
			{
				switch(mode)
				{
					case SEEK_CUR:
						return Core::InputStream::SeekType::Relative;

					case SEEK_END:
						return Core::InputStream::SeekType::FromEnd;

					case SEEK_SET:
					default:
						return Core::InputStream::SeekType::Absolute;
				}
			}();

			readid->stream->Seek(seekType, delta);
			return 0;
		}
		catch(...)
		{
			return -1;
		}
	};

	streamReader.push_back_byte = [](void* id, int c)
	{
		auto readid = static_cast<ReadId*>(id);
		readid->ungottenc = static_cast<char>(c);
		return c;
	};

	streamReader.get_length = [](void* id)
	{
		auto readid = static_cast<ReadId*>(id);
		try
		{
			auto len = readid->stream->Length();
			if(len > std::numeric_limits<std::uint32_t>::max())
			{
				return static_cast<std::uint32_t>(-1);
			}
			else
			{
				return static_cast<std::uint32_t>(len);
			}
		}
		catch(...)
		{
			return static_cast<std::uint32_t>(-1);
		}
	};

	streamReader.can_seek = [](void* id)
	{
		auto readid = static_cast<ReadId*>(id);
		return static_cast<int>(readid->stream->CanSeek());
	};


	char errorMessage[80];
	WavpackContext* ctx = WavpackOpenFileInputEx(&streamReader, &readid, nullptr, errorMessage, 0, 0);


	int numChannels = WavpackGetNumChannels(ctx);
	int channelMask = WavpackGetChannelMask(ctx);
	int32_t sampleRate = WavpackGetSampleRate(ctx);
	int bytesPerSample = WavpackGetBytesPerSample(ctx);
	int bitsPerSample = WavpackGetBitsPerSample(ctx);

	if(numChannels > std::numeric_limits<std::uint16_t>::max() || numChannels < 0)
	{
		WavpackCloseFile(ctx);
		throw ConversionError("Number of channels is invalid.");
	}

	if(sampleRate < 0)
	{
		WavpackCloseFile(ctx);
		throw ConversionError("Sample rate is negative.");
	}

	if(bytesPerSample > std::numeric_limits<std::uint16_t>::max() / 8 || bytesPerSample < 0)
	{
		WavpackCloseFile(ctx);
		throw ConversionError("Bytes per sample is invalid.");
	}

	if(bitsPerSample > std::numeric_limits<std::uint16_t>::max() || bitsPerSample < 0)
	{
		WavpackCloseFile(ctx);
		throw ConversionError("Bits per sample is invalid.");
	}

	try
	{
		WAV::WAVWriter wavWriter(
			static_cast<std::uint16_t>(numChannels),
			static_cast<std::uint32_t>(sampleRate),
			static_cast<std::uint16_t>(bytesPerSample),
			static_cast<std::uint16_t>(bitsPerSample),
			static_cast<WAV::WAVFormatChunk::Speaker>(channelMask));

		Core::BinaryWriterLE writer(*streams.Output);
		wavWriter.WriteHeaders(writer);

		std::vector<std::int32_t> buffer(static_cast<std::size_t>(sampleRate) * numChannels);

		while(uint32_t samplesRead = WavpackUnpackSamples(ctx, &buffer[0], static_cast<std::uint32_t>(sampleRate)))
		{
			wavWriter.WriteSamples(writer, &buffer[0], static_cast<std::size_t>(samplesRead) * numChannels);
		}
	}
	catch(...)
	{
		WavpackCloseFile(ctx);
		throw;
	}

	WavpackCloseFile(ctx);
}


} /* namespace WavPack */
} /* namespace Plugin */
} /* namespace Neon */
