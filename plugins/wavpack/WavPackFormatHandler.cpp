/*
 * WavPackFormatHandler.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "WavPackFormatHandler.h"
#include "plugins/wav/WAVFormatHandler.h"
#include "plugins/wavpack/WavPackDecoder.h"
#include "plugins/wavpack/WavPackEncoder.h"

#include "core/BufferCompare.h"

#include <wavpack/wavpack.h>

#include <boost/format.hpp>

#include <exception>
#include <cstdio>

namespace Neon {
namespace Plugin {
namespace WavPack {

WavPackFormatHandler::WavPackFormatHandler()
{
	outputFormats["wv"] = WavPackFormat();
}

WavPackFormatHandler::~WavPackFormatHandler()
{
}

std::string WavPackFormatHandler::GetName() const
{
	return "WavPack";
}

std::vector<std::tuple<std::string, Core::FormatType>> WavPackFormatHandler::IdentifyInput(const std::string& path) const
{
	char errorMessage[80];
	WavpackContext* ctx = WavpackOpenFileInput(path.c_str(), errorMessage, 0, 0);

	if(ctx == nullptr)
	{
		return std::vector<std::tuple<std::string, Core::FormatType>>();
	}

	int numChannels = WavpackGetNumChannels(ctx);
	//int channelMask = WavpackGetChannelMask(ctx);
	uint32_t sampleRate = WavpackGetSampleRate(ctx);
	int bitDepth = WavpackGetBitsPerSample(ctx);

	WavpackCloseFile(ctx);

	return
	{
		std::make_tuple(
			path,
			WavPackFormat((boost::format("%1% Channels, %2%-bit, %3%Hz") % numChannels % bitDepth % sampleRate).str())
		)
	};
}

std::vector<Core::ConversionInfo> WavPackFormatHandler::GetConversions(const Core::FormatType& source) const
{
	std::vector<Core::ConversionInfo> ret = FormatHandler::GetConversions(source);

	/*if(source.GetContainer() == Core::FormatType::FormatContainer::WavPack &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::WavPack)
	{
		ret.push_back(Core::ConversionInfo(source, WAV::WAVFormatHandler::WavFormat(), 0, shared_from_this()));
	}
	else */if(source.GetContainer() == Core::FormatType::Container::Wave &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
	{
		ret.push_back(Core::ConversionInfo(source, WavPackFormat(), 0, shared_from_this()));
	}

	return ret;
}

std::shared_ptr<Core::FormatCodec> WavPackFormatHandler::ConvertStream(const Core::ConversionInfo& info) const
{
	const auto& source = info.GetSource();
	const auto& result = info.GetResult();

	/*if(source.GetContainer() == Core::FormatType::FormatContainer::WavPack &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::WavPack &&

		result.GetContainer() == Core::FormatType::FormatContainer::Wave &&
		result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
	{
		return std::shared_ptr<Core::FormatCodec>(new WavPackDecoder());
	}
	else */if(source.GetContainer() == Core::FormatType::Container::Wave &&
			source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM &&

			result.GetContainer() == Core::FormatType::Container::WavPack &&
			result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::WavPack)
	{
		return std::shared_ptr<Core::FormatCodec>(new WavPackEncoder());
	}
	else
	{
		return FormatHandler::ConvertStream(info);
	}
}

Core::FormatType WavPackFormatHandler::WavPackFormat(std::string info)
{
	return Core::FormatType(
		Core::FormatType::Container::WavPack,
		std::vector<Core::TrackInfo> { Core::TrackInfo(Core::TrackInfo::Codec::WavPack, info) }
	);
}


Core::FormatHandlerFactory<WavPackFormatHandler> factory;

} /* namespace WavPack */
} /* namespace Plugin */
} /* namespace Neon */
