hasWavPack = try_compile(['wavpack'],
"
#include \"wavpack/wavpack.h\"

int main()
{
	WavpackOpenFileInput(0, 0, 0, 0);
	return 0;
}

")
PluginInfo.new(
	enabled: hasWavPack,
	dependencies: ['wav'],
	libraries: ['wavpack']
)	

