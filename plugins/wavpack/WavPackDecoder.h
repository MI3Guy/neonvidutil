/*
 * WavPackDecoder.h
 *
 *  Created on: Mar 30, 2014
 *      Author: john
 */

#pragma once

#include "core/FormatCodec.h"

namespace Neon {
namespace Plugin {
namespace WavPack {

class WavPackDecoder : public Core::FormatCodec {
public:
	WavPackDecoder();
	virtual ~WavPackDecoder();

	virtual std::string GetName() const override;


	virtual void ConvertData(Core::IOStreamPair streams) override;

private:

	std::shared_ptr<Core::InputStream> inputStream;
	std::shared_ptr<Core::OutputStream> outputStream;



};

} /* namespace WavPack */
} /* namespace Plugin */
} /* namespace Neon */
