/*
 * WavPackFormatHandler.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#pragma once

#include "core/FormatHandler.h"
#include "core/FormatType.h"

#include <boost/optional.hpp>
#include <string>
#include <memory>


namespace Neon {
namespace Plugin {
namespace WavPack {

class WavPackFormatHandler : public Core::FormatHandler, public std::enable_shared_from_this<WavPackFormatHandler> {
public:
	WavPackFormatHandler();
	virtual ~WavPackFormatHandler();

	virtual std::string GetName() const override;
	virtual std::vector<std::tuple<std::string, Core::FormatType>> IdentifyInput(const std::string& path) const override;

	virtual std::vector<Core::ConversionInfo> GetConversions(const Core::FormatType& source) const override;
	virtual std::shared_ptr<Core::FormatCodec> ConvertStream(const Core::ConversionInfo& info) const override;


	class FlacError {};

private:
	static Core::FormatType WavPackFormat(std::string info = std::string());
};

} /* namespace WavPack */
} /* namespace Plugin */
} /* namespace Neon */
