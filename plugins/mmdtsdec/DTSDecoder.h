/*
 * DTSDecoder.h
 *
 *  Created on: Mar 30, 2014
 *      Author: john
 */

#pragma once

#include "core/FormatCodec.h"
#include "core/InputToSocket.h"
//#include "core/Process.h"
#include "core/ProcessStream.h"

namespace Neon {
namespace Plugin {
namespace mmdtsdec {

class DTSDecoder : public Core::FormatCodec {
public:
	DTSDecoder();
	virtual ~DTSDecoder();

	virtual std::string GetName() const override;

	virtual Core::IOStreamPair InitConvertData(std::shared_ptr<Core::InputStream> inStream);
	virtual std::shared_ptr<Core::OutputStream> InitConvertData(std::shared_ptr<Core::InputStream> inStream, const std::string& file);

	virtual void ConvertData(Core::IOStreamPair streams) override;

private:
	//std::shared_ptr<Core::InputToSocket> inputToSocket;
	//std::shared_ptr<Core::Process> process;
	std::shared_ptr<Core::ProcessStream> process;

	bool usedFile = false;

};

} /* namespace FLAC */
} /* namespace Plugin */
} /* namespace Neon */
