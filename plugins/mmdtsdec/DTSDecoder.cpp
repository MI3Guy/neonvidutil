/*
 * DTSDecoder.cpp
 *
 *  Created on: Mar 30, 2014
 *      Author: john
 */

#include "DTSDecoder.h"

#include <sys/socket.h>
#include <netinet/in.h>

#include <limits>
#include <boost/format.hpp>

//#include "core/SocketInputStream.h"
#include "core/TempFileInputStream.h"

namespace Neon {
namespace Plugin {
namespace mmdtsdec {

DTSDecoder::DTSDecoder() {
}

DTSDecoder::~DTSDecoder() {
}

std::string DTSDecoder::GetName() const
{
	return "DTS => WAV";
}

/*static std::uint8_t HeaderInfo[] =
{
	0x00, 0x00, 0x00, 0x96, 0xdf, 0xcf, 0x50, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};*/

Core::IOStreamPair DTSDecoder::InitConvertData(std::shared_ptr<Core::InputStream> inStream)
{
#if 0
	inputToSocket = std::shared_ptr<Core::InputToSocket>(new Core::InputToSocket(inStream, HeaderInfo, sizeof(HeaderInfo)));
	std::shared_ptr<Core::SocketInputStream> inputStream(new Core::SocketInputStream());
	std::this_thread::sleep_for(std::chrono::milliseconds(5000));
	process = std::shared_ptr<Core::Process>(new Core::Process(
		"strace",
		"mmdtsdec",
		"-v",
		"--framing", "3",
		"-d", "dtsdecoderdll.dll",
		(boost::format("tcp://:%1%") % inputToSocket->GetPort()).str(),
		(boost::format("tcp://:%1%") % inputStream->GetPort()).str()
	));

	return Core::IOStreamPair(inputStream, std::shared_ptr<Core::OutputStream>());
#else
#if 1
	process = std::shared_ptr<Core::ProcessStream>(new Core::ProcessStream(
		Core::ProcessStream::RedirectStandardInput | Core::ProcessStream::RedirectStandardOutput | Core::ProcessStream::RedirectStandardError,
		"mmdtsdec",
		"-d", "dtsdecoderdll.dll",
		Core::ProcessStream::StandardInputArgument,
		Core::ProcessStream::StandardOutputArgument
	));

	return Core::IOStreamPair(process->CreateInputStream(), process->CreateOutputStream());
#else

	std::string inFile = inStream->GetAsFile();

	std::shared_ptr<Core::TempFileInputStream> inputStream(new Core::TempFileInputStream());

	process = std::shared_ptr<Core::Process>(new Core::Process(
		"mmdtsdec",
		"-v",
		"-d", "dtsdecoderdll.dll",
		inFile,
		inputStream->GetPath()
	));

	process->Wait();
	return Core::IOStreamPair(inputStream, std::shared_ptr<Core::OutputStream>());
#endif
#endif
}

std::shared_ptr<Core::OutputStream> DTSDecoder::InitConvertData(std::shared_ptr<Core::InputStream> inStream, const std::string& file)
{
#if 0
	inputToSocket = std::shared_ptr<Core::InputToSocket>(new Core::InputToSocket(inStream, HeaderInfo, sizeof(HeaderInfo)));
	std::this_thread::sleep_for(std::chrono::milliseconds(5000));
	process = std::shared_ptr<Core::Process>(new Core::Process(
		"mmdtsdec",
		"-v",
		"--framing", "3",
		"-d", "dtsdecoderdll.dll",
		(boost::format("tcp://:%1%") % inputToSocket->GetPort()).str(),
		file
	));

	return std::shared_ptr<Core::OutputStream>();
#else
#if 1
	process = std::shared_ptr<Core::ProcessStream>(new Core::ProcessStream(
		Core::ProcessStream::RedirectStandardInput | Core::ProcessStream::RedirectStandardError,
		"mmdtsdec",
		"-d", "dtsdecoderdll.dll",
		Core::ProcessStream::StandardInputArgument,
		file
	));

	usedFile = true;

	return process->CreateOutputStream();
#else
	std::string inFile = inStream->GetAsFile();

	process = std::shared_ptr<Core::Process>(new Core::Process(
		"mmdtsdec",
		"-v",
		"-d", "dtsdecoderdll.dll",
		inFile,
		file
	));
	process->Wait();
	return std::shared_ptr<Core::OutputStream>();
#endif
#endif
}

void DTSDecoder::ConvertData(Core::IOStreamPair streams)
{
	//inputToSocket->WaitForStreamEnd();
	streams.Input->CopyTo(streams.Output);
	streams.Output->Close();
	auto code = process->Wait();

	auto stderrStream = process->CreateErrorInputStream();

	char buffer[0x1000];
	stderrStream->Read(buffer, sizeof(buffer));

	if(!code || (*code != 0 && (usedFile || *code != 1)))
	{

		throw ConversionError("mmdtsdec return an error code");
	}
}


} /* namespace FLAC */
} /* namespace Plugin */
} /* namespace Neon */
