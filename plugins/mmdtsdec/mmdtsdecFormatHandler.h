/*
 * mmdtsdecFormatHandler.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#pragma once

#include "core/FormatHandler.h"
#include "core/FormatType.h"

#include <boost/optional.hpp>
#include <string>
#include <memory>


namespace Neon {
namespace Plugin {
namespace mmdtsdec {

class mmdtsdecFormatHandler : public Core::FormatHandler, public std::enable_shared_from_this<mmdtsdecFormatHandler> {
public:
	mmdtsdecFormatHandler();
	virtual ~mmdtsdecFormatHandler();

	virtual std::string GetName() const override;

	virtual std::vector<Core::ConversionInfo> GetConversions(const Core::FormatType& source) const override;
	virtual std::shared_ptr<Core::FormatCodec> ConvertStream(const Core::ConversionInfo& info) const override;

private:
	static Core::FormatType WavFormat();
};

} /* namespace FLAC */
} /* namespace Plugin */
} /* namespace Neon */
