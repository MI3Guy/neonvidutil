/*
 * mmdtsdecFormatHandler.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "mmdtsdecFormatHandler.h"

#include "DTSDecoder.h"

#include "core/BufferCompare.h"

#include <boost/format.hpp>

#include <exception>
#include <cstdio>

namespace Neon {
namespace Plugin {
namespace mmdtsdec {

mmdtsdecFormatHandler::mmdtsdecFormatHandler()
{
	outputFormats["wav"] = WavFormat();
}

mmdtsdecFormatHandler::~mmdtsdecFormatHandler()
{
}

std::string mmdtsdecFormatHandler::GetName() const
{
	return "mmdtsdec";
}

std::vector<Core::ConversionInfo> mmdtsdecFormatHandler::GetConversions(const Core::FormatType& source) const
{
	std::vector<Core::ConversionInfo> ret = FormatHandler::GetConversions(source);

	if(source.GetContainer() == Core::FormatType::Container::DTS &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::DTSHDMA)
	{
		ret.push_back(Core::ConversionInfo(source, WavFormat(), 0, shared_from_this()));
	}

	return ret;
}

std::shared_ptr<Core::FormatCodec> mmdtsdecFormatHandler::ConvertStream(const Core::ConversionInfo& info) const
{
	const auto& source = info.GetSource();
	const auto& result = info.GetResult();

	if(source.GetContainer() == Core::FormatType::Container::DTS &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::DTSHDMA &&

		result.GetContainer() == Core::FormatType::Container::Wave &&
		result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
	{
		return std::shared_ptr<Core::FormatCodec>(new DTSDecoder());
	}
	else
	{
		return FormatHandler::ConvertStream(info);
	}
}


Core::FormatType mmdtsdecFormatHandler::WavFormat()
{
	return Core::FormatType(
		Core::FormatType::Container::Wave,
		std::vector<Core::TrackInfo> { Core::TrackInfo(Core::TrackInfo::Codec::PCM) }
	);
}

Core::FormatHandlerFactory<mmdtsdecFormatHandler> factory;

} /* namespace FLAC */
} /* namespace Plugin */
} /* namespace Neon */
