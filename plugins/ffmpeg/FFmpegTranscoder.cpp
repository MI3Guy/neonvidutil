/*
 * FFmpegTranscoder.cpp
 *
 *  Created on: Mar 30, 2014
 *      Author: john
 */

#include "plugins/ffmpeg/FFmpegTranscoder.h"

#include "plugins/wav/WAVReader.h"

#include <vector>
#include <boost/format.hpp>

namespace Neon {
namespace Plugin {
namespace FFmpeg {

FFmpegTranscoder::FFmpegTranscoder(std::string inputCodec_, std::string inputFormat_, std::string outputCodec_, std::string outputFormat_, int streamIndex_)
	: inputCodec(inputCodec_), inputFormat(inputFormat_), outputCodec(outputCodec_), outputFormat(outputFormat_), streamIndex(streamIndex_)
{
}

FFmpegTranscoder::~FFmpegTranscoder()
{
}

std::string FFmpegTranscoder::GetName() const
{
	return "FFmpeg";
}

void FFmpegTranscoder::ConvertData(Core::IOStreamPair streams)
{
	// Setup Input Stream

	AVInputFormat* inputFormatStruct = av_find_input_format(inputFormat.c_str());
	if(inputFormatStruct == nullptr)
	{
		throw ConversionError((boost::format("An error occurred while finding input format %1%.") % inputFormat).str());
	}

	AVFormatContext* inputFormatCtx = avformat_alloc_context();
	if(inputFormatCtx == nullptr)
	{
		throw ConversionError("Could not allocate input context.");
	}

	const int BufferSize = 4*1024;
	unsigned char* inputBuffer = static_cast<unsigned char*>(av_malloc(BufferSize));
	if(inputBuffer == nullptr)
	{
		throw ConversionError("Could not allocate input buffer.");
	}

	struct InputOpaque
	{
		std::shared_ptr<Core::InputStream> inputStream;
		bool hasError;
	} inputOpaque = { streams.Input, false };


	auto readInputFunc = [](void* opaque, uint8_t* buf, int buf_size)
	{
		auto& opaqueObj = *static_cast<InputOpaque*>(opaque);
		try
		{
			return static_cast<int>(opaqueObj.inputStream->Read(buf, buf_size));
		}
		catch(...)
		{
			opaqueObj.hasError = true;
			return 0;
		}
	};

	auto seekInputFunc = [](void* opaque, int64_t offset, int whence)
	{
		whence = whence & ~AVSEEK_FORCE;
		auto& opaqueObj = *static_cast<InputOpaque*>(opaque);

		try
		{
			if(whence == AVSEEK_SIZE)
			{
				return opaqueObj.inputStream->Length();
			}

			auto seekType = [&]()
			{
				switch(whence)
				{
					case SEEK_CUR:
						return Core::InputStream::SeekType::Relative;

					case SEEK_END:
						return Core::InputStream::SeekType::FromEnd;

					case SEEK_SET:
					default:
						return Core::InputStream::SeekType::Absolute;
				}
			}();

			return opaqueObj.inputStream->Seek(seekType, offset);
		}
		catch(...)
		{
			opaqueObj.hasError = true;
			return static_cast<Core::InputStream::LengthType>(-1);
		}
	};

	auto seekInputFuncPtr = [&]()
	{
		if(streams.Input->CanSeek())
		{
			return static_cast<int64_t(*)(void*, int64_t, int)>(seekInputFunc);
		}
		else
		{
			return static_cast<int64_t(*)(void*, int64_t, int)>(nullptr);
		}
	}();

	inputFormatCtx->pb = avio_alloc_context(inputBuffer, BufferSize, 0, &inputOpaque, readInputFunc, nullptr, seekInputFuncPtr);
	if(inputFormatCtx->pb == nullptr)
	{
		av_free(inputBuffer);
		throw ConversionError("Could not allocate input avio context.");
	}

	if(avformat_open_input(&inputFormatCtx, "", inputFormatStruct, nullptr) != 0)
	{
		av_free(inputFormatCtx->pb);
		av_free(inputBuffer);
		throw ConversionError("Could not open input.");
	}


	// Setup Output Stream

	struct OutputOpaque
	{
		std::shared_ptr<Core::OutputStream> outputStream;
		bool hasError;
	} outputOpaque = { streams.Output, false };

	auto writeOutputFunc = [](void* opaque, uint8_t* buf, int buf_size)
	{
		auto& opaqueObj = *static_cast<OutputOpaque*>(opaque);
		try
		{
			opaqueObj.outputStream->Write(buf, buf_size);
			return buf_size;
		}
		catch(...)
		{
			opaqueObj.hasError = true;
			return 0;
		}
	};

	auto seekOutputFunc = [](void* opaque, int64_t offset, int whence)
	{
		whence = whence & ~AVSEEK_FORCE;
		auto& opaqueObj = *static_cast<OutputOpaque*>(opaque);

		try
		{
			if(whence == AVSEEK_SIZE)
			{
				return opaqueObj.outputStream->Length();
			}

			auto seekType = [&]()
			{
				switch(whence)
				{
					case SEEK_CUR:
						return Core::InputStream::SeekType::Relative;

					case SEEK_END:
						return Core::InputStream::SeekType::FromEnd;

					case SEEK_SET:
					default:
						return Core::InputStream::SeekType::Absolute;
				}
			}();

			return opaqueObj.outputStream->Seek(seekType, offset);
		}
		catch(...)
		{
			opaqueObj.hasError = true;
			return static_cast<Core::InputStream::LengthType>(-1);
		}
	};

	auto seekOutputFuncPtr = [&]()
	{
		if(streams.Output->CanSeek())
		{
			return static_cast<int64_t(*)(void*, int64_t, int)>(seekOutputFunc);
		}
		else
		{
			return static_cast<int64_t(*)(void*, int64_t, int)>(nullptr);
		}
	}();


	AVFormatContext* outputFormatCtx = nullptr;
	{
		int err = avformat_alloc_output_context2(&outputFormatCtx, nullptr, outputFormat.c_str(), nullptr);
		if(outputFormatCtx == nullptr || err < 0)
		{
			av_free(inputFormatCtx->pb);
			av_free(inputBuffer);
			throw ConversionError((boost::format("Could not open output. Error: ") % err).str());
		}
	}

	unsigned char* outputBuffer = nullptr;
	outputFormatCtx->pb = nullptr;

	if(!(outputFormatCtx->oformat->flags & AVFMT_NOFILE))
	{
		outputBuffer = static_cast<unsigned char*>(av_malloc(BufferSize));
		if(outputBuffer == nullptr)
		{
			av_free(inputFormatCtx->pb);
			av_free(inputBuffer);
			throw ConversionError("Could not allocate output buffer.");
		}

		outputFormatCtx->pb = avio_alloc_context(outputBuffer, BufferSize, 1, &outputOpaque, nullptr, writeOutputFunc, seekOutputFuncPtr);
		if(outputFormatCtx->pb == nullptr)
		{
			av_free(outputBuffer);
			av_free(inputFormatCtx->pb);
			av_free(inputBuffer);
			throw ConversionError("Could not allocate output buffer.");

		}
	}

	// Convert!

	AVCodecID codecId = CODEC_ID_NONE;

	// Prepare inFmt
	if(avformat_find_stream_info(inputFormatCtx, nullptr) < 0) {
		av_free(outputBuffer);
		av_free(inputFormatCtx->pb);
		av_free(inputBuffer);
		throw ConversionError("Could not find stream info.");
	}

	// Prepare inCodec
	AVMediaType mediaType = AVMEDIA_TYPE_AUDIO;
	AVCodec* inCodec = NULL;
	int realStreamIndex = av_find_best_stream(inputFormatCtx, AVMEDIA_TYPE_AUDIO, streamIndex, -1, &inCodec, 0);
	if(realStreamIndex < 0) {
		realStreamIndex = av_find_best_stream(inputFormatCtx, AVMEDIA_TYPE_VIDEO, streamIndex, -1, &inCodec, 0);
		mediaType = AVMEDIA_TYPE_VIDEO;
		if(realStreamIndex < 0) {
			av_free(outputBuffer);
			av_free(inputFormatCtx->pb);
			av_free(inputBuffer);
			throw ConversionError("Could not find input stream.");
		}
	}

	AVCodecContext* inCodecCtx = inputFormatCtx->streams[realStreamIndex]->codec;
	AVCodec* outCodec = NULL;

	if(outputCodec != "copy") {
		outCodec = DetermineCodec(outputCodec, codecId, inCodecCtx->bits_per_coded_sample);
		if(outCodec == NULL) {
			av_free(outputBuffer);
			av_free(inputFormatCtx->pb);
			av_free(inputBuffer);
			throw ConversionError("Could not find codec.");
		}
	}
	else
	{
		outCodec = inCodec;
	}

	AVStream* outStream = avformat_new_stream(outputFormatCtx, outCodec);
	if(!outStream) {
		av_free(outputBuffer);
		av_free(inputFormatCtx->pb);
		av_free(inputBuffer);
		throw ConversionError("Could not create stream.");
	}

	if(outputFormatCtx->oformat->flags & AVFMT_GLOBALHEADER) {
		outStream->codec->flags |= CODEC_FLAG_GLOBAL_HEADER;
	}

	if(outputCodec != "copy") {
		if(mediaType != AVMEDIA_TYPE_AUDIO) {
			av_free(outputBuffer);
			av_free(inputFormatCtx->pb);
			av_free(inputBuffer);
			throw ConversionError("Could not convert unknown media type.");
		}
		else {
			try
			{
				ConvertFFmpegAudio(inputFormatCtx, inCodecCtx, inCodec, outputFormatCtx, outStream, outCodec, realStreamIndex);
			}
			catch(...)
			{
				av_free(outputBuffer);
				av_free(inputFormatCtx->pb);
				av_free(inputBuffer);
				throw;
			}
		}
	}
	else {
		if(mediaType == AVMEDIA_TYPE_AUDIO)
		{
			outStream->id = 1;
			outStream->codec->codec = outCodec;
			outStream->codec->codec_id = inCodecCtx->codec_id;
			outStream->codec->sample_fmt = inCodecCtx->sample_fmt;
			outStream->codec->sample_rate = inCodecCtx->sample_rate;
			outStream->codec->channels = inCodecCtx->channels;
			outStream->codec->channel_layout = inCodecCtx->channel_layout;
		}

		ConvertFFmpegCopy(inputFormatCtx, inCodecCtx, outputFormatCtx, outStream, realStreamIndex);
	}

	// Cleanup
	avcodec_close(inCodecCtx);

	for(decltype(outputFormatCtx->nb_streams) i = 0; i < outputFormatCtx->nb_streams; ++i) {
		av_freep(&outputFormatCtx->streams[i]->codec);
		av_freep(&outputFormatCtx->streams[i]);
	}


	av_free(outputFormatCtx->pb);
	av_free(outputBuffer);
	//avformat_free_context(outputFormatCtx);
	av_free(inputFormatCtx->pb);
	av_free(inputBuffer);
	//avformat_free_context(inputFormatCtx);
}

AVCodec* FFmpegTranscoder::DetermineCodec(const std::string& codec, AVCodecID codecId, int bitDepth)
{
	if(codec.empty())
	{
		return avcodec_find_encoder(codecId);
	}
	else if(codec == "pcm")
	{
		if(bitDepth <= 8)
		{
			return avcodec_find_encoder(CODEC_ID_PCM_U8);
		}
		else if(bitDepth <= 16)
		{
			return avcodec_find_encoder(CODEC_ID_PCM_S16LE);
		}
		else if(bitDepth <= 24)
		{
			return avcodec_find_encoder(CODEC_ID_PCM_S24LE);
		}
		else if(bitDepth <= 32)
		{
			return avcodec_find_encoder(CODEC_ID_PCM_S32LE);
		}
		else
		{
			return nullptr;
		}
	}
	else
	{
		return avcodec_find_encoder_by_name(codec.c_str());
	}
}

void FFmpegTranscoder::ConvertFFmpegAudio(AVFormatContext* inFmt, AVCodecContext* inCodecCtx, AVCodec* inCodec, AVFormatContext* outFmt, AVStream* audioStream, AVCodec* outCodec, int streamIndex)
{
	if(avcodec_open2(inCodecCtx, inCodec, NULL) < 0)
	{
		throw ConversionError("Could not open input codec.");
	}

	audioStream->id = 1;
	audioStream->codec->sample_fmt = inCodecCtx->sample_fmt;
	audioStream->codec->sample_rate = inCodecCtx->sample_rate;
	audioStream->codec->channels = inCodecCtx->channels;
	audioStream->codec->channel_layout = inCodecCtx->channel_layout;

	if(avcodec_open2(audioStream->codec, outCodec, NULL) < 0)
	{
		throw ConversionError("Could not open output codec.");
	}

	if(avformat_write_header(outFmt, NULL) != 0)
	{
		throw ConversionError("Could not write header.");
	}

	AVPacket inPacket {};
	AVPacket outPacket {};
	av_init_packet(&inPacket);
	av_init_packet(&outPacket);

	AVFrame* frame = av_frame_alloc();
	if(frame == NULL)
	{
		throw ConversionError("Error allocating frame.");
	}

	while(av_read_frame(inFmt, &inPacket) == 0)
	{
		if(inPacket.stream_index == streamIndex)
		{
			int got_frame_ptr = 0;
			while(got_frame_ptr == 0)
			{
				av_frame_unref(frame);

				if(avcodec_decode_audio4(inCodecCtx, frame, &got_frame_ptr, &inPacket) < 0)
				{
					av_frame_unref(frame);
					av_free(frame);
					throw ConversionError("Error while decoding.");
				}
			}

			int got_packet_ptr = 0;
			while(got_packet_ptr == 0) {
				int errorCode;
				if((errorCode = avcodec_encode_audio2(audioStream->codec, &outPacket, frame, &got_packet_ptr)) != 0) {
					av_frame_unref(frame);
					av_free(frame);
					throw ConversionError((boost::format("Error while encoding (errorCode = %1%).") % errorCode).str());
				}
			}

			if(av_interleaved_write_frame(outFmt, &outPacket) != 0) {
				av_frame_unref(frame);
				av_free(frame);
				throw ConversionError("Error while writing frame.");
			}

			av_free_packet(&outPacket);
		}

		av_free_packet(&inPacket);
	}

	av_frame_unref(frame);
	av_free(frame);
	av_write_trailer(outFmt);
}

void FFmpegTranscoder::ConvertFFmpegCopy(AVFormatContext* inFmt, AVCodecContext* inCodecCtx, AVFormatContext* outFmt, AVStream* outStream, int streamIndex)
{
	if(avformat_write_header(outFmt, NULL) < 0)
	{
		throw ConversionError("Error while writing header.");
	}

	AVPacket packet {};
	av_init_packet(&packet);

	while(av_read_frame(inFmt, &packet) == 0)
	{
		if(packet.stream_index == streamIndex)
		{
			auto inStream = inFmt->streams[packet.stream_index];

			packet.pts = av_rescale_q_rnd(packet.pts, inStream->time_base, outStream->time_base, static_cast<AVRounding>(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
			packet.dts = av_rescale_q_rnd(packet.dts, inStream->time_base, outStream->time_base, static_cast<AVRounding>(AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
			//packet.pts = av_rescale_rnd(packet.pts, inStream->time_base, outStream->time_base, AV_ROUND_NEAR_INF/*|AV_ROUND_PASS_MINMAX*/);
			//packet.dts = av_rescale_rnd(packet.dts, inStream->time_base, outStream->time_base, AV_ROUND_NEAR_INF/*|AV_ROUND_PASS_MINMAX*/);
			packet.duration = static_cast<int>(av_rescale_q(packet.duration, inStream->time_base, outStream->time_base));
			packet.pos = -1;

			if(av_interleaved_write_frame(outFmt, &packet) < 0)
			{
				av_free_packet(&packet);
				throw ConversionError("Error muxing packet");
			}
		}
		av_free_packet(&packet);
	}
}

} /* namespace FLAC */
} /* namespace Plugin */
} /* namespace Neon */
