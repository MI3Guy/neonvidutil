/*
 * FFmpegFormatHandler.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#pragma once

#include "core/FormatHandler.h"
#include "core/FormatType.h"

#include <boost/optional.hpp>
#include <string>
#include <memory>


namespace Neon {
namespace Plugin {
namespace FFmpeg {

class FFmpegFormatHandler : public Core::FormatHandler, public std::enable_shared_from_this<FFmpegFormatHandler> {
public:
	FFmpegFormatHandler();
	virtual ~FFmpegFormatHandler();

	virtual std::string GetName() const override;
	virtual std::vector<std::tuple<std::string, Core::FormatType>> IdentifyInput(const std::string& path) const override;

	virtual std::vector<Core::ConversionInfo> GetConversions(const Core::FormatType& source) const override;
	virtual std::shared_ptr<Core::FormatCodec> ConvertStream(const Core::ConversionInfo& info) const override;


	class FlacError {};

private:
	static Core::FormatType FlacFormat(std::string info = std::string());
};

} /* namespace FLAC */
} /* namespace Plugin */
} /* namespace Neon */
