=begin
hasFlac = try_compile(['FLAC'],
"
#include \"FLAC/stream_decoder.h\"
#include \"FLAC/stream_encoder.h\"

int main()
{
	FLAC__stream_decoder_new();
	FLAC__stream_encoder_new();
	return 0;
}

")

=end
PluginInfo.new(
	enabled: true,
#	dependencies: ['wav'],
	libraries: ['avformat', 'avcodec', 'avutil']
#	-lX11 -lasound -lSDL -lx264 -lvorbisenc -lvorbis -logg -lfaac -lass -lm -lz -pthread -lrt 
)	

