/*
 * FFmpegFormatHandler.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "FFmpegFormatHandler.h"
#include "plugins/wav/WAVFormatHandler.h"
#include "plugins/ffmpeg/FFmpegTranscoder.h"

#include "core/BufferCompare.h"

extern "C"
{
#include <libavformat/avformat.h>
}

#include <boost/format.hpp>

#include <exception>
#include <cstdio>

namespace Neon {
namespace Plugin {
namespace FFmpeg {

FFmpegFormatHandler::FFmpegFormatHandler()
{
	outputFormats["thd"] = Core::FormatType::Simple(Core::FormatType::Container::TrueHD, Core::TrackInfo::Codec::TrueHD);
	outputFormats["wav"] = Core::FormatType::Simple(Core::FormatType::Container::Wave, Core::TrackInfo::Codec::PCM);
	outputFormats["wv"] = Core::FormatType::Simple(Core::FormatType::Container::WavPack, Core::TrackInfo::Codec::WavPack);
	outputFormats["ac3"] = Core::FormatType::Simple(Core::FormatType::Container::AC3, Core::TrackInfo::Codec::AC3);
	outputFormats["eac3"] = Core::FormatType::Simple(Core::FormatType::Container::EAC3, Core::TrackInfo::Codec::EAC3);

	av_register_all();
}

FFmpegFormatHandler::~FFmpegFormatHandler()
{
}

std::string FFmpegFormatHandler::GetName() const
{
	return "FFmpeg";
}

std::vector<std::tuple<std::string, Core::FormatType>> FFmpegFormatHandler::IdentifyInput(const std::string& path) const
{
	AVFormatContext* fmtCtx = nullptr;
	if(avformat_open_input(&fmtCtx, path.c_str(), nullptr, nullptr) < 0)
	{
		return std::vector<std::tuple<std::string, Core::FormatType>>();
	}

	if(avformat_find_stream_info(fmtCtx, nullptr) < 0)
	{
		avformat_free_context(fmtCtx);
		return std::vector<std::tuple<std::string, Core::FormatType>>();
	}

	const Core::FormatType::Container container = [&]()
	{
		if(strcmp(fmtCtx->iformat->name, "matroska,webm") == 0)
		{
			return Core::FormatType::Container::Matroska;
		}
		else if(strcmp(fmtCtx->iformat->name, "vc1") == 0)
		{
			return Core::FormatType::Container::VC1;
		}
		else if(strcmp(fmtCtx->iformat->name, "mpegvideo") == 0)
		{
			return Core::FormatType::Container::MPEGVideo;
		}
		else if(strcmp(fmtCtx->iformat->name, "wav") == 0)
		{
			return Core::FormatType::Container::Wave;
		}
		else if(strcmp(fmtCtx->iformat->name, "flac") == 0)
		{
			return Core::FormatType::Container::FLAC;
		}
		else if(strcmp(fmtCtx->iformat->name, "wv") == 0)
		{
			return Core::FormatType::Container::WavPack;
		}
		else if(strcmp(fmtCtx->iformat->name, "truehd") == 0)
		{
			return Core::FormatType::Container::TrueHD;
		}
		else if(strcmp(fmtCtx->iformat->name, "ac3") == 0)
		{
			return Core::FormatType::Container::AC3;
		}
		else if(strcmp(fmtCtx->iformat->name, "eac3") == 0)
		{
			return Core::FormatType::Container::EAC3;
		}
		else if(strcmp(fmtCtx->iformat->name, "dts") == 0)
		{
			return Core::FormatType::Container::DTS;
		}
		else
		{
			return Core::FormatType::Container::Unknown;
		}
	}();

	if(container == Core::FormatType::Container::Unknown)
	{
		avformat_free_context(fmtCtx);
		return std::vector<std::tuple<std::string, Core::FormatType>>();
	}

	std::vector<Core::TrackInfo> tracks;
	tracks.reserve(fmtCtx->nb_streams);

	for(decltype(fmtCtx->nb_streams) i = 0; i < fmtCtx->nb_streams; ++i)
	{
		const Core::TrackInfo::Codec codec = [&]()
		{
			switch(fmtCtx->streams[i]->codec->codec_id)
			{
				// Video
				case CODEC_ID_H264:
					return Core::TrackInfo::Codec::AVC;

				case CODEC_ID_VC1:
					return Core::TrackInfo::Codec::VC1;

				case CODEC_ID_MPEG2VIDEO:
					return Core::TrackInfo::Codec::MPEG2Video;

				// Audio
				case CODEC_ID_PCM_S24LE:
				case CODEC_ID_PCM_S16LE:
					return Core::TrackInfo::Codec::PCM;

				case CODEC_ID_FLAC:
					return Core::TrackInfo::Codec::FLAC;

				case CODEC_ID_WAVPACK:
					return Core::TrackInfo::Codec::WavPack;

				case CODEC_ID_TRUEHD:
					return Core::TrackInfo::Codec::TrueHD;

				case CODEC_ID_AC3:
					return Core::TrackInfo::Codec::AC3;

				case CODEC_ID_EAC3:
					return Core::TrackInfo::Codec::EAC3;

				case CODEC_ID_DTS:
					switch(fmtCtx->streams[i]->codec->profile)
					{
						case FF_PROFILE_DTS:
							return Core::TrackInfo::Codec::DTS;

						//case FF_PROFILE_DTS_ES:
						//	return Core::TrackInfo::Codec::DTS;

						case FF_PROFILE_DTS_HD_HRA:
							return Core::TrackInfo::Codec::DTSHDHR;

						case FF_PROFILE_DTS_HD_MA:
							return Core::TrackInfo::Codec::DTSHDMA;

						default:
							return Core::TrackInfo::Codec::Unknown;
					}

				default:
					return Core::TrackInfo::Codec::Unknown;
			}
		}();

		tracks.push_back(Core::TrackInfo(codec, "", i));
	}

	avformat_free_context(fmtCtx);

	return
	{
		std::make_tuple(
			path,
			Core::FormatType(container, tracks))
	};
}

std::vector<Core::ConversionInfo> FFmpegFormatHandler::GetConversions(const Core::FormatType& source) const
{
	std::vector<Core::ConversionInfo> ret = FormatHandler::GetConversions(source);

	if(source.GetContainer() == Core::FormatType::Container::Matroska)
	{
		for(auto track : source.GetTracks())
		{
			switch(track.GetCodec())
			{
				case Core::TrackInfo::Codec::PCM:
					ret.push_back(Core::ConversionInfo(source,
						Core::FormatType(Core::FormatType::Container::Wave, { Core::TrackInfo(Core::TrackInfo::Codec::PCM) }),
						track.GetTrackId(), shared_from_this()));
					break;

				case Core::TrackInfo::Codec::TrueHD:
					ret.push_back(Core::ConversionInfo(source,
						Core::FormatType(Core::FormatType::Container::Wave, { Core::TrackInfo(Core::TrackInfo::Codec::PCM) }),
						track.GetTrackId(), shared_from_this()));

					ret.push_back(Core::ConversionInfo(source,
						Core::FormatType(Core::FormatType::Container::TrueHD, { Core::TrackInfo(Core::TrackInfo::Codec::TrueHD) }),
						track.GetTrackId(), shared_from_this()));
					break;

				case Core::TrackInfo::Codec::WavPack:
					ret.push_back(Core::ConversionInfo(source,
						Core::FormatType(Core::FormatType::Container::Wave, { Core::TrackInfo(Core::TrackInfo::Codec::PCM) }),
						track.GetTrackId(), shared_from_this()));

					ret.push_back(Core::ConversionInfo(source,
						Core::FormatType(Core::FormatType::Container::WavPack, { Core::TrackInfo(Core::TrackInfo::Codec::WavPack) }),
						track.GetTrackId(), shared_from_this()));
					break;

				case Core::TrackInfo::Codec::AC3:
					ret.push_back(Core::ConversionInfo(source,
						Core::FormatType(Core::FormatType::Container::Wave, { Core::TrackInfo(Core::TrackInfo::Codec::PCM) }),
						track.GetTrackId(), shared_from_this()));

					ret.push_back(Core::ConversionInfo(source,
						Core::FormatType(Core::FormatType::Container::AC3, { Core::TrackInfo(Core::TrackInfo::Codec::AC3) }),
						track.GetTrackId(), shared_from_this()));
					break;

				case Core::TrackInfo::Codec::EAC3:
					ret.push_back(Core::ConversionInfo(source,
						Core::FormatType(Core::FormatType::Container::Wave, { Core::TrackInfo(Core::TrackInfo::Codec::PCM) }),
						track.GetTrackId(), shared_from_this()));

					ret.push_back(Core::ConversionInfo(source,
						Core::FormatType(Core::FormatType::Container::EAC3, { Core::TrackInfo(Core::TrackInfo::Codec::EAC3) }),
						track.GetTrackId(), shared_from_this()));
					break;

				default:
					break;
			}
		}
	}
	else if(source.GetContainer() == Core::FormatType::Container::TrueHD &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::TrueHD)
	{
		ret.push_back(Core::ConversionInfo(source,
			Core::FormatType(Core::FormatType::Container::Wave, { Core::TrackInfo(Core::TrackInfo::Codec::PCM) }),
			0, shared_from_this()));
	}
	else if(source.GetContainer() == Core::FormatType::Container::WavPack &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::WavPack)
	{
		ret.push_back(Core::ConversionInfo(source,
			Core::FormatType(Core::FormatType::Container::Wave, { Core::TrackInfo(Core::TrackInfo::Codec::PCM) }),
			0, shared_from_this()));
	}
	else if(source.GetContainer() == Core::FormatType::Container::AC3 &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::AC3)
	{
		ret.push_back(Core::ConversionInfo(source,
			Core::FormatType(Core::FormatType::Container::Wave, { Core::TrackInfo(Core::TrackInfo::Codec::PCM) }),
			0, shared_from_this()));
	}
	else if(source.GetContainer() == Core::FormatType::Container::EAC3 &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::EAC3)
	{
		ret.push_back(Core::ConversionInfo(source,
			Core::FormatType(Core::FormatType::Container::Wave, { Core::TrackInfo(Core::TrackInfo::Codec::PCM) }),
			0, shared_from_this()));
	}
	else if(source.GetContainer() == Core::FormatType::Container::Wave &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
	{
		ret.push_back(Core::ConversionInfo(source,
			Core::FormatType(Core::FormatType::Container::AC3, { Core::TrackInfo(Core::TrackInfo::Codec::AC3) }),
			0, shared_from_this()));
	}

	/*else if(source.GetContainer() == Core::FormatType::FormatContainer::Wave &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
	{
		ret.push_back(Core::ConversionInfo(source, FlacFormat(), 0, shared_from_this()));
	}*/

	return ret;
}

std::shared_ptr<Core::FormatCodec> FFmpegFormatHandler::ConvertStream(const Core::ConversionInfo& info) const
{
	const auto& source = info.GetSource();
	const auto& result = info.GetResult();

	if(source.GetContainer() == Core::FormatType::Container::Matroska && result.GetTracks().size() == 1)
	{
		if(source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
		{
			if(result.GetContainer() == Core::FormatType::Container::Wave && result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
			{
				return std::shared_ptr<Core::FormatCodec>(new FFmpegTranscoder("pcm", "matroska", "copy", "wav", info.GetStreamIndex()));
			}
		}
		else if(source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::TrueHD)
		{
			if(result.GetContainer() == Core::FormatType::Container::Wave && result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
			{
				return std::shared_ptr<Core::FormatCodec>(new FFmpegTranscoder("truehd", "matroska", "pcm_s24le", "wav", info.GetStreamIndex()));
			}
			else if(result.GetContainer() == Core::FormatType::Container::TrueHD && result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::TrueHD)
			{
				return std::shared_ptr<Core::FormatCodec>(new FFmpegTranscoder("truehd", "matroska", "copy", "truehd", info.GetStreamIndex()));
			}
		}
		else if(source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::WavPack)
		{
			if(result.GetContainer() == Core::FormatType::Container::Wave && result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
			{
				return std::shared_ptr<Core::FormatCodec>(new FFmpegTranscoder("wavpack", "matroska", "pcm", "wav", info.GetStreamIndex()));
			}
			else if(result.GetContainer() == Core::FormatType::Container::TrueHD && result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::TrueHD)
			{
				return std::shared_ptr<Core::FormatCodec>(new FFmpegTranscoder("wavpack", "matroska", "copy", "wv", info.GetStreamIndex()));
			}
		}
		else if(source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::AC3)
		{
			if(result.GetContainer() == Core::FormatType::Container::Wave && result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
			{
				return std::shared_ptr<Core::FormatCodec>(new FFmpegTranscoder("ac3", "matroska", "pcm", "wav", info.GetStreamIndex()));
			}
			else if(result.GetContainer() == Core::FormatType::Container::AC3 && result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::AC3)
			{
				return std::shared_ptr<Core::FormatCodec>(new FFmpegTranscoder("ac3", "matroska", "copy", "ac3", info.GetStreamIndex()));
			}
		}
		else if(source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::EAC3)
		{
			if(result.GetContainer() == Core::FormatType::Container::Wave && result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
			{
				return std::shared_ptr<Core::FormatCodec>(new FFmpegTranscoder("eac3", "matroska", "pcm", "wav", info.GetStreamIndex()));
			}
			else if(result.GetContainer() == Core::FormatType::Container::EAC3 && result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::EAC3)
			{
				return std::shared_ptr<Core::FormatCodec>(new FFmpegTranscoder("eac3", "matroska", "copy", "eac3", info.GetStreamIndex()));
			}
		}
	}
	else if(source.GetContainer() == Core::FormatType::Container::TrueHD &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::TrueHD &&

		result.GetContainer() == Core::FormatType::Container::Wave &&
		result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
	{
		return std::shared_ptr<Core::FormatCodec>(new FFmpegTranscoder("truehd", "truehd", "pcm_s24le", "wav", info.GetStreamIndex()));
	}
	else if(source.GetContainer() == Core::FormatType::Container::WavPack &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::WavPack &&

		result.GetContainer() == Core::FormatType::Container::Wave &&
		result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)

	{
		return std::shared_ptr<Core::FormatCodec>(new FFmpegTranscoder("wavpack", "wv", "pcm", "wav", info.GetStreamIndex()));
	}
	else if(source.GetContainer() == Core::FormatType::Container::AC3 &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::AC3 &&

		result.GetContainer() == Core::FormatType::Container::Wave &&
		result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
	{
		return std::shared_ptr<Core::FormatCodec>(new FFmpegTranscoder("ac3", "ac3", "pcm_s16le", "wav", info.GetStreamIndex()));
	}
	else if(source.GetContainer() == Core::FormatType::Container::EAC3 &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::EAC3 &&

		result.GetContainer() == Core::FormatType::Container::Wave &&
		result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
	{
		return std::shared_ptr<Core::FormatCodec>(new FFmpegTranscoder("eac3", "eac3", "pcm_s16le", "wav", info.GetStreamIndex()));
	}
	/*else if(source.GetContainer() == Core::FormatType::Container::Wave &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM &&

		result.GetContainer() == Core::FormatType::Container::AC3 &&
		result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::AC3)
	{
		return std::shared_ptr<Core::FormatCodec>(new FFmpegTranscoder("pcm", "wav", "ac3_fixed", "ac3", info.GetStreamIndex()));
	}*/

	return FormatHandler::ConvertStream(info);
}


Core::FormatHandlerFactory<FFmpegFormatHandler> factory;

} /* namespace FLAC */
} /* namespace Plugin */
} /* namespace Neon */
