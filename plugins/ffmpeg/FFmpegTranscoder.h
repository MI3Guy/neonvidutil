/*
 * FFmpegTranscoder.h
 *
 *  Created on: Mar 30, 2014
 *      Author: john
 */

#pragma once

#include "core/FormatCodec.h"

extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
}


namespace Neon {
namespace Plugin {
namespace FFmpeg {

class FFmpegTranscoder : public Core::FormatCodec {
public:
	FFmpegTranscoder(std::string inputCodec_, std::string inputFormat_, std::string outputCodec_, std::string outputFormat_, int streamIndex_);
	virtual ~FFmpegTranscoder();

	virtual std::string GetName() const override;


	virtual void ConvertData(Core::IOStreamPair streams) override;

private:
	std::string inputCodec;
	std::string inputFormat;
	std::string outputCodec;
	std::string outputFormat;
	int streamIndex;
	std::shared_ptr<Core::InputStream> inputStream;
	std::shared_ptr<Core::OutputStream> outputStream;

	static AVCodec* DetermineCodec(const std::string& string, AVCodecID codecId, int bitDepth);

	void ConvertFFmpegAudio(AVFormatContext* inFmt, AVCodecContext* inCodecCtx, AVCodec* inCodec, AVFormatContext* outFmt, AVStream* audioStream, AVCodec* outCodec, int streamIndex);
	void ConvertFFmpegCopy(AVFormatContext* inFmt, AVCodecContext* inCodecCtx, AVFormatContext* outFmt, AVStream* outStream, int streamIndex);

};

} /* namespace FLAC */
} /* namespace Plugin */
} /* namespace Neon */
