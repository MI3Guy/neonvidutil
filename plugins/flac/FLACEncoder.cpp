/*
 * FLACEncoder.cpp
 *
 *  Created on: Mar 30, 2014
 *      Author: john
 */

#include "plugins/flac/FLACEncoder.h"

#include "plugins/wav/WAVReader.h"

#include <FLAC/stream_encoder.h>
#include <vector>
#include <limits>

namespace Neon {
namespace Plugin {
namespace FLAC {

FLACEncoder::FLACEncoder()
{
}

FLACEncoder::~FLACEncoder()
{
}

std::string FLACEncoder::GetName() const
{
	return "WAV => FLAC";
}

void FLACEncoder::ConvertData(Core::IOStreamPair streams)
{
	Core::BinaryReaderLE reader(*streams.Input);
	WAV::WAVReader wavReader(reader);

	FLAC__StreamEncoder* encoder = FLAC__stream_encoder_new();

	if(encoder == nullptr)
	{
		throw ConversionError("Unable to create FLAC encoder.");
	}

	FLAC__stream_encoder_set_channels(encoder, wavReader.GetFormat().nChannels);
	FLAC__stream_encoder_set_bits_per_sample(encoder, wavReader.GetFormat().wBitsPerSample);
	FLAC__stream_encoder_set_sample_rate(encoder, wavReader.GetFormat().nSamplesPerSec);

	FLAC__stream_encoder_set_compression_level(encoder, 8);
	FLAC__stream_encoder_set_verify(encoder, true);

	auto writeCallback = [](const FLAC__StreamEncoder* encoder, const FLAC__byte buffer[], size_t bytes, unsigned samples, unsigned current_frame, void* client_data)
	{
		try
		{
			static_cast<Core::OutputStream*>(client_data)->Write(buffer, bytes);
			return FLAC__STREAM_ENCODER_WRITE_STATUS_OK;
		}
		catch(...)
		{
			return FLAC__STREAM_ENCODER_WRITE_STATUS_FATAL_ERROR;
		}
	};

	auto seekCallback = [](const FLAC__StreamEncoder* encoder, FLAC__uint64 absolute_byte_offset, void* client_data)
	{
		auto& stream = *static_cast<Core::OutputStream*>(client_data);
		if(!stream.CanSeek())
		{
			return FLAC__STREAM_ENCODER_SEEK_STATUS_UNSUPPORTED;
		}
		else
		{
			try
			{
				if(sizeof(Core::InputStream::LengthType) >= sizeof(FLAC__uint64) ||
					absolute_byte_offset <= static_cast<FLAC__uint64>(std::numeric_limits<Core::InputStream::LengthType>::max()))
				{
					stream.Seek(static_cast<Core::OutputStream::LengthType>(absolute_byte_offset));
					return FLAC__STREAM_ENCODER_SEEK_STATUS_OK;
				}
				else
				{
					return FLAC__STREAM_ENCODER_SEEK_STATUS_ERROR;
				}
			}
			catch(...)
			{
				return FLAC__STREAM_ENCODER_SEEK_STATUS_ERROR;
			}
		}
	};

	auto tellCallback = [](const FLAC__StreamEncoder* encoder, FLAC__uint64* absolute_byte_offset, void* client_data)
	{
		auto& stream = *static_cast<Core::OutputStream*>(client_data);

		if(!stream.CanSeek())
		{
			return FLAC__STREAM_ENCODER_TELL_STATUS_UNSUPPORTED;
		}
		else
		{
			try
			{
				Core::InputStream::LengthType pos = stream.Position();

				if(sizeof(FLAC__uint64) >= sizeof(Core::InputStream::LengthType) ||
					pos <= static_cast<Core::InputStream::LengthType>(std::numeric_limits<FLAC__uint64>::max()))
				{
					*absolute_byte_offset = pos;
					return FLAC__STREAM_ENCODER_TELL_STATUS_OK;
				}
				else
				{
					return FLAC__STREAM_ENCODER_TELL_STATUS_ERROR;
				}
			}
			catch(...)
			{
				return FLAC__STREAM_ENCODER_TELL_STATUS_ERROR;
			}
		}
	};

	FLAC__StreamEncoderInitStatus status = FLAC__stream_encoder_init_stream(encoder, writeCallback, seekCallback, tellCallback, nullptr, streams.Output.get());

	if(status != FLAC__STREAM_ENCODER_INIT_STATUS_OK)
	{
		FLAC__stream_encoder_delete(encoder);
		throw ConversionError(FLAC__StreamEncoderInitStatusString[status]);
	}

	const auto numChannels = wavReader.GetFormat().nChannels;

	const std::size_t bufferSize = [&]()
	{
		std::size_t ret = numChannels * wavReader.GetFormat().nSamplesPerSec;
		if(ret <= std::numeric_limits<unsigned>::max())
		{
			return ret;
		}
		else
		{
			return static_cast<std::size_t>(numChannels);
		}
	}();

	try
	{
		std::vector<FLAC__int32> buffer(bufferSize);

		while(std::size_t numRead = wavReader.ReadSamples(&buffer[0], buffer.size()))
		{
			if(!FLAC__stream_encoder_process_interleaved(encoder, &buffer[0], static_cast<unsigned>(numRead / numChannels)))
			{
				std::string error = FLAC__StreamEncoderStateString[FLAC__stream_encoder_get_state(encoder)];
				FLAC__stream_encoder_delete(encoder);
				throw ConversionError(error);
			}
		}
	}
	catch(...)
	{
		FLAC__stream_encoder_delete(encoder);
		throw;
	}

	if(!FLAC__stream_encoder_finish(encoder))
	{
		std::string error = FLAC__StreamEncoderStateString[FLAC__stream_encoder_get_state(encoder)];
		FLAC__stream_encoder_delete(encoder);
		throw ConversionError(error);
	}

	FLAC__stream_encoder_delete(encoder);
}


} /* namespace FLAC */
} /* namespace Plugin */
} /* namespace Neon */
