/*
 * FLACDecoder.cpp
 *
 *  Created on: Mar 30, 2014
 *      Author: john
 */

#include "FLACDecoder.h"

#include "plugins/wav/WAVWriter.h"

#include <FLAC/stream_decoder.h>

#include <limits>

namespace Neon {
namespace Plugin {
namespace FLAC {

FLACDecoder::FLACDecoder() {
	// TODO Auto-generated constructor stub

}

FLACDecoder::~FLACDecoder() {
	// TODO Auto-generated destructor stub
}

std::string FLACDecoder::GetName() const
{
	return "FLAC => WAV";
}

void FLACDecoder::ConvertData(Core::IOStreamPair streams)
{
	struct ClientData
	{
		Core::IOStreamPair streams;
		//FLACDecoder* self;
		Core::BinaryWriterLE writer;
		std::shared_ptr<WAV::WAVWriter> wavWriter;
	} clientData = { streams, {*streams.Output} };


	FLAC__StreamDecoder* decoder = FLAC__stream_decoder_new();

	if(decoder == nullptr)
	{
		throw ConversionError("Unable to create FLAC decoder.");
	}

	FLAC__stream_decoder_set_md5_checking(decoder, true);

	auto readCallback = [](const FLAC__StreamDecoder* decoder, FLAC__byte buffer[], size_t* bytes, void* client_data)
	{
		std::shared_ptr<Core::InputStream> stream = static_cast<ClientData*>(client_data)->streams.Input;

		if(*bytes > 0)
		{
			try
			{
				*bytes = stream->Read(buffer, *bytes);

				if(*bytes == 0)
				{
					return FLAC__STREAM_DECODER_READ_STATUS_END_OF_STREAM;
				}
				else
				{
					return FLAC__STREAM_DECODER_READ_STATUS_CONTINUE;
				}
			}
			catch(...)
			{
				return FLAC__STREAM_DECODER_READ_STATUS_ABORT;
			}
		}
		else
		{
			return FLAC__STREAM_DECODER_READ_STATUS_ABORT;
		}
	};

	auto seekCallback = [](const FLAC__StreamDecoder* decoder, FLAC__uint64 absolute_byte_offset, void* client_data)
	{
		std::shared_ptr<Core::InputStream> stream = static_cast<ClientData*>(client_data)->streams.Input;

		if(!stream->CanSeek())
		{
			return FLAC__STREAM_DECODER_SEEK_STATUS_UNSUPPORTED;
		}
		else
		{
			try
			{
				if(sizeof(Core::InputStream::LengthType) >= sizeof(FLAC__uint64) ||
					absolute_byte_offset <= static_cast<FLAC__uint64>(std::numeric_limits<Core::InputStream::LengthType>::max()))
				{
					stream->Seek(static_cast<Core::InputStream::LengthType>(absolute_byte_offset));
					return FLAC__STREAM_DECODER_SEEK_STATUS_OK;
				}
				else
				{
					return FLAC__STREAM_DECODER_SEEK_STATUS_ERROR;
				}
			}
			catch(...)
			{
				return FLAC__STREAM_DECODER_SEEK_STATUS_ERROR;
			}
		}
	};

	auto tellCallback = [](const FLAC__StreamDecoder* decoder, FLAC__uint64* absolute_byte_offset, void* client_data)
	{
		std::shared_ptr<Core::InputStream> stream = static_cast<ClientData*>(client_data)->streams.Input;

		if(!stream->CanSeek())
		{
			return FLAC__STREAM_DECODER_TELL_STATUS_UNSUPPORTED;
		}
		else
		{
			try
			{
				Core::InputStream::LengthType pos = stream->Position();

				if(sizeof(FLAC__uint64) >= sizeof(Core::InputStream::LengthType) ||
					pos <= static_cast<Core::InputStream::LengthType>(std::numeric_limits<FLAC__uint64>::max()))
				{
					*absolute_byte_offset = static_cast<FLAC__uint64>(pos);
					return FLAC__STREAM_DECODER_TELL_STATUS_OK;
				}
				else
				{
					return FLAC__STREAM_DECODER_TELL_STATUS_ERROR;
				}
			}
			catch(...)
			{
				return FLAC__STREAM_DECODER_TELL_STATUS_ERROR;
			}
		}
	};

	auto lengthCallback = [](const FLAC__StreamDecoder* decoder, FLAC__uint64* stream_length, void* client_data)
	{
		std::shared_ptr<Core::InputStream> stream = static_cast<ClientData*>(client_data)->streams.Input;
		if(!stream->CanSeek())
		{
			return FLAC__STREAM_DECODER_LENGTH_STATUS_UNSUPPORTED;
		}
		else
		{
			try
			{
				Core::InputStream::LengthType length = stream->Position();

				if(sizeof(FLAC__uint64) >= sizeof(Core::InputStream::LengthType) ||
						length <= static_cast<Core::InputStream::LengthType>(std::numeric_limits<FLAC__uint64>::max()))
				{
					*stream_length = static_cast<FLAC__uint64>(length);
					return FLAC__STREAM_DECODER_LENGTH_STATUS_OK;
				}
				else
				{
					return FLAC__STREAM_DECODER_LENGTH_STATUS_ERROR;
				}
			}
			catch(...)
			{
				return FLAC__STREAM_DECODER_LENGTH_STATUS_ERROR;
			}
		}
	};

	auto eofCallback = [](const FLAC__StreamDecoder* decoder, void* clent_data)
	{
		return static_cast<FLAC__bool>(false);
	};

	auto writeCallback = [](const FLAC__StreamDecoder* decoder, const FLAC__Frame* frame, const FLAC__int32* const buffer[], void* client_data)
	{
		auto& clientData = *static_cast<ClientData*>(client_data);
		std::shared_ptr<Core::OutputStream> stream = clientData.streams.Output;

		try
		{
			if(!clientData.wavWriter)
			{
				if(frame->header.channels > std::numeric_limits<std::uint16_t>::max() ||
					frame->header.sample_rate > std::numeric_limits<std::uint32_t>::max() ||
					frame->header.bits_per_sample > std::numeric_limits<std::uint16_t>::max())
				{
					return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
				}

				std::uint16_t channels = static_cast<std::uint16_t>(frame->header.channels);
				std::uint32_t samplesPerSec = frame->header.sample_rate;
				std::uint16_t bytesPerSample = static_cast<std::uint16_t>((frame->header.bits_per_sample / 8) + ((frame->header.bits_per_sample % 8 > 0) ? 1 : 0));
				std::uint16_t validBitsPerSample = static_cast<std::uint16_t>(frame->header.bits_per_sample);
				WAV::WAVFormatChunk::Speaker speakerLayout = [&]()
				{
					switch(channels)
					{
						case 1:
							return WAV::WAVFormatChunk::Speaker::Mono;

						case 2:
							return WAV::WAVFormatChunk::Speaker::Stereo;

						case 3:
							return WAV::WAVFormatChunk::Speaker::FLACThreeChannel;

						case 4:
							return WAV::WAVFormatChunk::Speaker::Quad;

						case 5:
							return WAV::WAVFormatChunk::Speaker::FLACFiveChannel;

						case 6:
							return WAV::WAVFormatChunk::Speaker::FivePointOne;

						case 7:
						default:
							return WAV::WAVFormatChunk::Speaker::None;



					}
				}();

				std::shared_ptr<WAV::WAVWriter> wavWriter(new WAV::WAVWriter(channels, samplesPerSec, bytesPerSample, validBitsPerSample, speakerLayout));
				wavWriter->WriteHeaders(clientData.writer);
				clientData.wavWriter = wavWriter;
			}

			for(decltype(frame->header.blocksize) i = 0; i < frame->header.blocksize; ++i)
			{
				for(decltype(frame->header.channels) j = 0; j < frame->header.channels; ++j)
				{
					clientData.wavWriter->WriteSamples(clientData.writer, &buffer[j][i], 1);
				}
			}

			return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
		}
		catch(...)
		{
			return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
		}

	};

	auto errorCallback = [](const FLAC__StreamDecoder* decoder, FLAC__StreamDecoderErrorStatus, void* client_data) {};

	FLAC__StreamDecoderInitStatus status = FLAC__stream_decoder_init_stream(
		decoder,
		readCallback,
		seekCallback,
		tellCallback,
		lengthCallback,
		eofCallback,
		writeCallback,
		nullptr,
		errorCallback,
		&clientData);

	if(status != FLAC__STREAM_DECODER_INIT_STATUS_OK)
	{
		FLAC__stream_decoder_delete(decoder);
		throw ConversionError(FLAC__StreamDecoderInitStatusString[status]);
	}

	if(!FLAC__stream_decoder_process_until_end_of_stream(decoder))
	{
		std::string errorMessage = FLAC__StreamDecoderStateString[FLAC__stream_decoder_get_state(decoder)];
		FLAC__stream_decoder_delete(decoder);
		throw ConversionError(errorMessage);
	}

	if(!FLAC__stream_decoder_finish(decoder))
	{
		// MD5 No matchey
	}

	FLAC__stream_decoder_delete(decoder);
}


} /* namespace FLAC */
} /* namespace Plugin */
} /* namespace Neon */
