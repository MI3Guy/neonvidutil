/*
 * FLACFormatHandler.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "FLACFormatHandler.h"
#include "plugins/wav/WAVFormatHandler.h"
#include "plugins/flac/FLACDecoder.h"
#include "plugins/flac/FLACEncoder.h"

#include "core/BufferCompare.h"

#include "FLAC/stream_decoder.h"

#include <boost/format.hpp>

#include <exception>
#include <cstdio>

namespace Neon {
namespace Plugin {
namespace FLAC {

FLACFormatHandler::FLACFormatHandler()
{
	outputFormats["flac"] = FlacFormat();
}

FLACFormatHandler::~FLACFormatHandler()
{
}

std::string FLACFormatHandler::GetName() const
{
	return "FLAC";
}

std::vector<std::tuple<std::string, Core::FormatType>> FLACFormatHandler::IdentifyInput(const std::string& path) const
{
	FILE* fp = fopen(path.c_str(), "r");
	if(fp == nullptr)
	{
		return std::vector<std::tuple<std::string, Core::FormatType>>();
	}

	char buffer[4];
	for(std::size_t totalRead = 0, numRead = 0; totalRead < sizeof(buffer) && (numRead = fread(buffer + totalRead, 1, sizeof(buffer) - totalRead, fp)) > 0; totalRead += numRead);
	fclose(fp);
	if(!Core::BufferEquals(buffer, FlacHeader, sizeof(buffer)))
	{
		return std::vector<std::tuple<std::string, Core::FormatType>>();
	}

	FLAC__StreamDecoder* decoder = FLAC__stream_decoder_new();

	if(decoder == nullptr)
	{
		return std::vector<std::tuple<std::string, Core::FormatType>>();
	}

#ifdef _WIN32
#error TODO: Implement
#endif

	bool hasFlacHeader = false;
	FLAC__FrameHeader header;

	auto capturingCallback = [&](const FLAC__StreamDecoder* decoder, const FLAC__Frame* frame, const FLAC__int32* const buffer[])
	{
		hasFlacHeader = true;
		header = frame->header;
		return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
	};

	FLAC__StreamDecoderWriteCallback writeCallback = [](const FLAC__StreamDecoder* decoder, const FLAC__Frame* frame, const FLAC__int32* const buffer[], void* client_data)
	{
		return (*static_cast<decltype(&capturingCallback)>(client_data))(decoder, frame, buffer);
	};
	FLAC__StreamDecoderErrorCallback errorCallback = [](const FLAC__StreamDecoder* decoder, FLAC__StreamDecoderErrorStatus, void* client_data) {};

	if(FLAC__stream_decoder_init_file(decoder, path.c_str(), writeCallback, nullptr, errorCallback, &capturingCallback))
	{
		FLAC__stream_decoder_delete(decoder);
		return std::vector<std::tuple<std::string, Core::FormatType>>();
	}

	FLAC__stream_decoder_process_until_end_of_stream(decoder);

	FLAC__stream_decoder_delete(decoder);

	if(hasFlacHeader)
	{
		return
		{
			std::make_tuple(
				path,
				FlacFormat((boost::format("%1% Channels, %2%-bit, %3%Hz") % header.channels % header.bits_per_sample % header.sample_rate).str())
			)
		};
	}
	else
	{
		return std::vector<std::tuple<std::string, Core::FormatType>>();
	}

}

std::vector<Core::ConversionInfo> FLACFormatHandler::GetConversions(const Core::FormatType& source) const
{
	std::vector<Core::ConversionInfo> ret = FormatHandler::GetConversions(source);

	if(source.GetContainer() == Core::FormatType::Container::FLAC &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::FLAC)
	{
		ret.push_back(Core::ConversionInfo(source, WAV::WAVFormatHandler::WavFormat(), 0, shared_from_this()));
	}
	else if(source.GetContainer() == Core::FormatType::Container::Wave &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
	{
		ret.push_back(Core::ConversionInfo(source, FlacFormat(), 0, shared_from_this()));
	}

	return ret;
}

std::shared_ptr<Core::FormatCodec> FLACFormatHandler::ConvertStream(const Core::ConversionInfo& info) const
{
	const auto& source = info.GetSource();
	const auto& result = info.GetResult();

	if(source.GetContainer() == Core::FormatType::Container::FLAC &&
		source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::FLAC &&

		result.GetContainer() == Core::FormatType::Container::Wave &&
		result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM)
	{
		return std::shared_ptr<Core::FormatCodec>(new FLACDecoder());
	}
	else if(source.GetContainer() == Core::FormatType::Container::Wave &&
			source.GetTracks().size() == 1 && source.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::PCM &&

			result.GetContainer() == Core::FormatType::Container::FLAC &&
			result.GetTracks().size() == 1 && result.GetTracks()[0].GetCodec() == Core::TrackInfo::Codec::FLAC)
	{
		return std::shared_ptr<Core::FormatCodec>(new FLACEncoder());
	}
	else
	{
		return FormatHandler::ConvertStream(info);
	}
}



constexpr char FLACFormatHandler::FlacHeader[];

Core::FormatType FLACFormatHandler::FlacFormat(std::string info)
{
	return Core::FormatType(
		Core::FormatType::Container::FLAC,
		std::vector<Core::TrackInfo> { Core::TrackInfo(Core::TrackInfo::Codec::FLAC, info) }
	);
}


Core::FormatHandlerFactory<FLACFormatHandler> factory;

} /* namespace FLAC */
} /* namespace Plugin */
} /* namespace Neon */
