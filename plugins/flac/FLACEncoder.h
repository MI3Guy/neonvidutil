/*
 * FLACEncoder.h
 *
 *  Created on: Mar 30, 2014
 *      Author: john
 */

#pragma once

#include "core/FormatCodec.h"

namespace Neon {
namespace Plugin {
namespace FLAC {

class FLACEncoder : public Core::FormatCodec {
public:
	FLACEncoder();
	virtual ~FLACEncoder();

	virtual std::string GetName() const override;


	virtual void ConvertData(Core::IOStreamPair streams) override;

private:

	std::shared_ptr<Core::InputStream> inputStream;
	std::shared_ptr<Core::OutputStream> outputStream;



};

} /* namespace FLAC */
} /* namespace Plugin */
} /* namespace Neon */
