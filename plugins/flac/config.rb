hasFlac = try_compile(['FLAC'],
"
#include \"FLAC/stream_decoder.h\"
#include \"FLAC/stream_encoder.h\"

int main()
{
	FLAC__stream_decoder_new();
	FLAC__stream_encoder_new();
	return 0;
}

")
PluginInfo.new(
	enabled: hasFlac,
	dependencies: ['wav'],
	libraries: ['FLAC']
)	

