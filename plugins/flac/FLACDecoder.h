/*
 * FLACDecoder.h
 *
 *  Created on: Mar 30, 2014
 *      Author: john
 */

#pragma once

#include "core/FormatCodec.h"

namespace Neon {
namespace Plugin {
namespace FLAC {

class FLACDecoder : public Core::FormatCodec {
public:
	FLACDecoder();
	virtual ~FLACDecoder();

	virtual std::string GetName() const override;


	virtual void ConvertData(Core::IOStreamPair streams) override;
};

} /* namespace FLAC */
} /* namespace Plugin */
} /* namespace Neon */
