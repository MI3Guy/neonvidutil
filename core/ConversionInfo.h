/*
 * ConversionInfo.h
 *
 *  Created on: Mar 15, 2014
 *      Author: john
 */

#ifndef CONVERSIONINFO_H_
#define CONVERSIONINFO_H_

#include "core/FormatType.h"
#include "core/FormatCodec.h"
#include <memory>

namespace Neon {
namespace Core {

class FormatHandler;

class ConversionInfo final {
public:
	enum class ConversionFlags
	{
		None = 0x00,
		RequiresTempFile = 0x01,
		Lossy = 0x02
	};

	ConversionInfo(FormatType source, FormatType result, int streamIndex, std::shared_ptr<const FormatHandler> handler, ConversionFlags flags = ConversionFlags::None);
	virtual ~ConversionInfo();

private:
	FormatType source;
	FormatType result;
	int streamIndex;
	std::shared_ptr<const FormatHandler> handler;
	ConversionFlags flags;

public:
	const FormatType& GetSource() const;
	const FormatType& GetResult() const;
	int GetStreamIndex() const;

	std::shared_ptr<FormatCodec> CreateCodec();
};

} /* namespace Core */
} /* namespace Neon */

#endif /* CONVERSIONINFO_H_ */
