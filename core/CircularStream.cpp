/*
 * CircularStream.cpp
 *
 *  Created on: Mar 30, 2014
 *      Author: john
 */

#include <core/CircularStream.h>

namespace Neon {
namespace Core {

CircularStream::CircularStream()
	: readBufferIndex(0), writeBufferIndex(0), numFullBuffers(0), finished(false)
{

}

CircularStream::~CircularStream()
{
}

std::shared_ptr<InputStream> CircularStream::CreateInputStream()
{
	return std::shared_ptr<InputStream>(new Reader(shared_from_this()));
}

std::shared_ptr<OutputStream> CircularStream::CreateOutputStream()
{
	return std::shared_ptr<OutputStream>(new Writer(shared_from_this()));
}

void CircularStream::WriteData(const void* buffer, std::size_t size)
{
	if(finished)
	{
		throw StreamClosed();
	}

	for(std::size_t bytesCopied = 0; bytesCopied < size; )
	{
		{
			std::unique_lock<std::mutex> lock(syncLock);
			if(numFullBuffers >= numBuffers)
			{
				bufferEmptied.wait(lock, [this] { return numFullBuffers < numBuffers; });
			}
		}

		const std::size_t numToCopy = std::min(size - bytesCopied, bufferSize - writeBufferPosition);

		void* dest = &dataBuffers[writeBufferIndex][writeBufferPosition];
		const void* src = static_cast<const uint8_t*>(buffer) + bytesCopied;

		std::memcpy(dest, src, numToCopy);

		writeBufferPosition += numToCopy;
		bytesCopied += numToCopy;

		if(writeBufferPosition >= bufferSize)
		{
			++writeBufferIndex;
			if(writeBufferIndex >= numBuffers)
			{
				writeBufferIndex = 0;
			}

			writeBufferPosition = 0;

			{
				std::unique_lock<std::mutex> lock(syncLock);
				++numFullBuffers;
			}

			bufferFilled.notify_one();
		}
	}
}


std::size_t CircularStream::ReadData(void* buffer, std::size_t size)
{
	std::size_t bytesCopied = 0;
	for(;;)
	{
		std::size_t numToCopy = std::min(size - bytesCopied, bufferSize - readBufferPosition);

		{
			std::unique_lock<std::mutex> lock(syncLock);
			if(!finished && numFullBuffers == 0)
			{
				bufferFilled.wait(lock, [this] { return finished || numFullBuffers != 0; });
			}

			if(numFullBuffers == 0)
			{
				return bytesCopied;
			}

			if(finished && numFullBuffers == 1)
			{
				// Since this is the last buffer, check for the number of bytes in this buffer to prevent reading bad data.
				numToCopy = std::min(numToCopy, writeBufferPosition - readBufferPosition);
			}
		}

		if(numToCopy == 0)
		{
			break;
		}

		void* const dest = static_cast<uint8_t*>(buffer) + bytesCopied;
		const void* const src = &dataBuffers[readBufferIndex][readBufferPosition];
		std::memcpy(dest, src, numToCopy);

		readBufferPosition += numToCopy;
		bytesCopied += numToCopy;

		if(readBufferPosition >= bufferSize)
		{
			++readBufferIndex;
			if(readBufferIndex >= numBuffers)
			{
				readBufferIndex = 0;
			}

			readBufferPosition = 0;

			{
				std::unique_lock<std::mutex> lock(syncLock);
				--numFullBuffers;
			}

			bufferEmptied.notify_one();
		}
	}
	return bytesCopied;
}

void CircularStream::DoneWriting()
{
	if(finished)
	{
		throw StreamClosed();
	}


	{
		std::unique_lock<std::mutex> lock(syncLock);
		if(writeBufferPosition > 0)
		{
			++numFullBuffers;
		}

		finished = true;
	}

	bufferFilled.notify_one();
}





CircularStream::Reader::Reader(std::shared_ptr<CircularStream> circular)
	: circularStream(circular)
{
}

std::size_t CircularStream::Reader::Read(void* buffer, std::size_t size)
{
	return circularStream->ReadData(buffer, size);
}

bool CircularStream::Reader::CanSeek() const noexcept
{
	return false;
}

CircularStream::Reader::LengthType CircularStream::Reader::Seek(LengthType pos)
{
	throw FileSeekFailed();
}

CircularStream::Reader::LengthType CircularStream::Reader::Seek(SeekType type, LengthType pos)
{
	throw FileSeekFailed();
}

CircularStream::Reader::LengthType CircularStream::Reader::Length()
{
	throw FileSeekFailed();
}

CircularStream::Reader::LengthType CircularStream::Reader::Position()
{
	throw FileSeekFailed();
}

void CircularStream::Reader::Close()
{
}


CircularStream::Writer::Writer(std::shared_ptr<CircularStream> circular)
	: circularStream(circular)
{
}

CircularStream::Writer::~Writer()
{
	circularStream->DoneWriting();
}

void CircularStream::Writer::Write(const void* buffer, std::size_t size)
{
	return circularStream->WriteData(buffer, size);
}

bool CircularStream::Writer::CanSeek() const noexcept
{
	return false;
}

CircularStream::Reader::LengthType CircularStream::Writer::Seek(LengthType pos)
{
	throw FileSeekFailed();
}

CircularStream::Reader::LengthType CircularStream::Writer::Seek(SeekType type, LengthType pos)
{
	throw FileSeekFailed();
}

CircularStream::Reader::LengthType CircularStream::Writer::Length()
{
	throw FileSeekFailed();
}

CircularStream::Reader::LengthType CircularStream::Writer::Position()
{
	throw FileSeekFailed();
}

void CircularStream::Writer::Close()
{
}

} /* namespace Core */
} /* namespace Neon */
