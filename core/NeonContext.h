/*
 * NeonContext.h
 *
 *  Created on: Mar 21, 2014
 *      Author: john
 */

#ifndef NEONCONTEXT_H_
#define NEONCONTEXT_H_

#include "core/FormatHandler.h"

namespace Neon {
namespace Core {

class NeonContext {
public:
	NeonContext();
	virtual ~NeonContext();

private:
	std::vector<std::shared_ptr<FormatHandler>> plugins;

public:
	std::shared_ptr<InputStream> OpenInput(const std::string& file) const;
	std::vector<std::tuple<std::string, FormatType>> IdentifyInputFile(const std::string& file) const;
	boost::optional<FormatType> IdentifyOutputFile(const std::string& file) const;
	std::vector<ConversionInfo> FindConversionTypes(const FormatType& source) const;
};

} /* namespace Core */
} /* namespace Neon */

#endif /* NEONCONTEXT_H_ */
