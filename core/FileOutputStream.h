/*
 * FileOutputStream.h
 *
 *  Created on: Mar 29, 2014
 *      Author: john
 */

#ifndef FILEOUTPUTSTREAM_H_
#define FILEOUTPUTSTREAM_H_

#include "core/OutputStream.h"

#include <exception>
#include <string>
#include <cstring>

namespace Neon {
namespace Core {

class FileOutputStream : public OutputStream
{
public:
	FileOutputStream(const std::string& name);
	virtual ~FileOutputStream();

	virtual void Write(const void* buffer, std::size_t size) override;
	virtual bool CanSeek() const noexcept override;
	virtual LengthType Seek(LengthType pos) override;
	virtual LengthType Seek(SeekType type, LengthType pos) override;
	virtual LengthType Length() override;
	virtual LengthType Position() override;
	virtual void Close() override;

private:
#ifdef _WIN32
#else
	int fd;
#endif
};

} /* namespace Core */
} /* namespace Neon */

#endif /* FILEOUTPUTSTREAM_H_ */
