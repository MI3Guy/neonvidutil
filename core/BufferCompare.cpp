/*
 * BufferCompare.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "BufferCompare.h"

#include <cinttypes>
#include <cstring>

namespace Neon {
namespace Core {

bool BufferEquals(const void* buff1, const void* buff2, std::size_t length)
{
	const std::uint8_t* buff1c = static_cast<const std::uint8_t*>(buff1);
	const std::uint8_t* buff2c = static_cast<const std::uint8_t*>(buff2);

	for(std::size_t i = 0; i < length; ++i)
	{
		if(buff1c[i] != buff2c[i])
		{
			return false;
		}
	}

	return true;
}


} /* namespace Core */
} /* namespace Neon */
