/*
 * lang.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#pragma once

#include <libintl.h>

namespace Neon {
namespace Core {

inline const char* operator"" _gt(const char* str, size_t length)
{
	return gettext(str);
}

inline const char* Translate(const char* singular, const char* plural, unsigned long int num)
{
	return ngettext(singular, plural, num);
}

}
}
