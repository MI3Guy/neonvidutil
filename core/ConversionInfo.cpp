/*
 * ConversionInfo.cpp
 *
 *  Created on: Mar 15, 2014
 *      Author: john
 */

#include "ConversionInfo.h"

#include "core/FormatHandler.h"

namespace Neon {
namespace Core {

ConversionInfo::ConversionInfo(FormatType source_, FormatType result_, int streamIndex_, std::shared_ptr<const FormatHandler> handler_, ConversionFlags flags_)
	: source(source_), result(result_), streamIndex(streamIndex_), handler(handler_), flags(flags_)
{
}

ConversionInfo::~ConversionInfo() {
}

const FormatType& ConversionInfo::GetSource() const
{
	return source;
}

const FormatType& ConversionInfo::GetResult() const
{
	return result;
}

int ConversionInfo::GetStreamIndex() const
{
	return streamIndex;
}

std::shared_ptr<FormatCodec> ConversionInfo::CreateCodec()
{
	return handler->ConvertStream(*this);
}

} /* namespace Core */
} /* namespace Neon */
