/*
 * PipeStream.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "PipeStream.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits>
#include <unistd.h>

namespace Neon {
namespace Core {

PipeStream::PipeStream()
{
	int pipefd[2];
	if(pipe2(pipefd, O_CLOEXEC) == -1)
	{
		throw IOStreamBase::FileOpenFailed();
	}

	readHandle = pipefd[0];
	writeHandle = pipefd[1];
}

PipeStream::PipeStream(int)
{
}


PipeStream::~PipeStream()
{
	if(readHandle != -1)
	{
		close(readHandle);
		readHandle = -1;
	}
	if(writeHandle != -1)
	{
		close(writeHandle);
		writeHandle = -1;
	}
}

std::shared_ptr<InputStream> PipeStream::CreateInputStream()
{
	std::shared_ptr<InputStream> ret(new PipeInputStream(readHandle));
	readHandle = -1;
	return ret;
}

std::shared_ptr<OutputStream> PipeStream::CreateOutputStream()
{
	std::shared_ptr<OutputStream> ret(new PipeOutputStream(writeHandle));
	writeHandle = -1;
	return ret;
}

PipeStream::PipeInputStream::PipeInputStream(int fd_)
	: fd(fd_)
{
	if(fd == -1)
	{
		throw FileOpenFailed();
	}
}

PipeStream::PipeInputStream::~PipeInputStream() {
	if(fd != -1)
	{
		close(fd);
		fd = -1;
	}
}

std::size_t PipeStream::PipeInputStream::Read(void* buffer, std::size_t size)
{
	if(size > static_cast<std::size_t>(std::numeric_limits<ssize_t>::max()))
	{
		size = std::numeric_limits<ssize_t>::max();
	}

	ssize_t numBytesRead = read(fd, buffer, size);
	if(numBytesRead < 0)
	{
		perror(nullptr);
		throw FileReadFailed();
	}

	return static_cast<std::size_t>(numBytesRead);
}

bool PipeStream::PipeInputStream::CanSeek() const noexcept
{
	return false;
}

PipeStream::PipeInputStream::LengthType PipeStream::PipeInputStream::Seek(LengthType pos)
{
	throw SeekingNotSupported();
}

PipeStream::PipeInputStream::LengthType PipeStream::PipeInputStream::Seek(SeekType type, LengthType pos)
{
	throw SeekingNotSupported();
}

PipeStream::PipeInputStream::LengthType PipeStream::PipeInputStream::Length()
{
	throw SeekingNotSupported();
}

PipeStream::PipeInputStream::LengthType PipeStream::PipeInputStream::Position()
{
	throw SeekingNotSupported();
}

void PipeStream::PipeInputStream::Close()
{
	if(fd != -1)
	{
		if(close(fd) < 0)
		{
			throw FileReadFailed();
		}
		fd = -1;
	}
}


PipeStream::PipeOutputStream::PipeOutputStream(int fd_)
	: fd(fd_)
{
	if(fd == -1)
	{
		throw FileOpenFailed();
	}
}

PipeStream::PipeOutputStream::~PipeOutputStream() {
	if(fd != -1)
	{
		close(fd);
		fd = -1;
	}
}

void PipeStream::PipeOutputStream::Write(const void* buffer, std::size_t size)
{
	const std::uint8_t* buffer2 = static_cast<const std::uint8_t*>(buffer);

	while(size > 0)
	{
		ssize_t numBytes = write(fd, buffer2, size);
		if(numBytes != -1)
		{
			buffer2 += numBytes;
			size -= numBytes;
		}
		else
		{
			if(!(errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR))
			{
				throw FileWriteFailed();
			}
		}
	}
}

bool PipeStream::PipeOutputStream::CanSeek() const noexcept
{
	return false;
}

PipeStream::PipeOutputStream::LengthType PipeStream::PipeOutputStream::Seek(LengthType pos)
{
	throw SeekingNotSupported();
}

PipeStream::PipeOutputStream::LengthType PipeStream::PipeOutputStream::Seek(SeekType type, LengthType pos)
{
	throw SeekingNotSupported();
}

PipeStream::PipeOutputStream::LengthType PipeStream::PipeOutputStream::Length()
{
	throw SeekingNotSupported();
}

PipeStream::PipeOutputStream::LengthType PipeStream::PipeOutputStream::Position()
{
	throw SeekingNotSupported();
}

void PipeStream::PipeOutputStream::Close()
{
	if(fd != -1)
	{
		if(close(fd) < 0)
		{
			throw FileWriteFailed();
		}
		fd = -1;
	}
}


} /* namespace Core */
} /* namespace Neon */
