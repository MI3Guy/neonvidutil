/*
 * IOStreamPair.cpp
 *
 *  Created on: Mar 31, 2014
 *      Author: john
 */

#include "core/IOStreamPair.h"

namespace Neon {
namespace Core {

IOStreamPair::IOStreamPair(std::shared_ptr<InputStream> inputStream, std::shared_ptr<OutputStream> outputStream)
	: Input(inputStream), Output(outputStream)
{
}

} /* namespace Core */
} /* namespace Neon */
