/*
 * FormatHandler.h
 *
 *  Created on: Mar 15, 2014
 *      Author: john
 */

#pragma once

#include "core/ConversionInfo.h"
#include "core/FormatType.h"

#include <boost/optional.hpp>
#include <map>
#include <string>
#include <vector>
#include <memory>

namespace Neon {
namespace Core {

class FormatHandler {
public:
	class UnsupportedConversion : std::exception {};

	FormatHandler();
	virtual ~FormatHandler();

	virtual std::string GetName() const = 0;

	virtual std::shared_ptr<InputStream> OpenInput(const std::string& path) const;

	virtual std::vector<std::tuple<std::string, FormatType>> IdentifyInput(const std::string& path) const;
	virtual boost::optional<FormatType> IdentifyOutput(const std::string& path) const;

	virtual std::vector<ConversionInfo> GetConversions(const FormatType& source) const;
	virtual std::shared_ptr<FormatCodec> ConvertStream(const ConversionInfo& info) const;

	//virtual std::shared_ptr<FormatCodec> CreateCodec(ConversionInfo conversion) = 0;
protected:
	std::map<std::string, FormatType> outputFormats; ///<Map from extension to FormatType. Extension should not include a '.'.
};

class FormatHandlerFactoryBase
{
protected:
	FormatHandlerFactoryBase()
	{
		GetFactories().push_back(this);
	}
	
	~FormatHandlerFactoryBase()
	{
		auto& factories = GetFactories();
		factories.erase(std::remove(factories.begin(), factories.end(), this), factories.end());
	}

	static std::vector<FormatHandlerFactoryBase*>& GetFactories()
	{
		static std::vector<FormatHandlerFactoryBase*> instance;
		return instance;
	}
	
public:
	virtual std::shared_ptr<FormatHandler> Create() = 0;

	static std::vector<std::shared_ptr<FormatHandler>> CreateHandlers()
	{
		std::vector<std::shared_ptr<FormatHandler>> ret;
		auto& factories = GetFactories();

		ret.reserve(factories.size());

		for(FormatHandlerFactoryBase* factory : factories)
		{
			ret.push_back(factory->Create());
		}

		return ret;
	}
};

template<typename HandlerType>
class FormatHandlerFactory final : public FormatHandlerFactoryBase
{
public:
	virtual std::shared_ptr<FormatHandler> Create() override
	{
		return std::shared_ptr<FormatHandler>(new HandlerType());
	}
};

} /* namespace Core */
} /* namespace Neon */

