#include "core/TrackInfo.h"

#include "core/lang.h"

#include <boost/format.hpp>

namespace Neon
{
namespace Core
{


std::string TrackInfo::FormatCodecTypeToString(Codec codec)
{
	switch(codec)
	{
		case Codec::Unknown:
			return "Unknown"_gt;
			
		case Codec::None:
			return "None"_gt;
			
		case Codec::AVC:
			return "AVC";
			
		case Codec::VC1:
			return "VC-1";
			
		case Codec::MPEG2Video:
			return "MPEG2 Video";
			
		case Codec::PCM:
			return "PCM";
			
		case Codec::FLAC:
			return "FLAC";
			
		case Codec::WavPack:
			return "WavPack";
			
		case Codec::TrueHD:
			return "TrueHD";
			
		case Codec::DTSHDMA:
			return "DTS-HD MA";

		case Codec::AC3:
			return "AC-3";
			
		case Codec::EAC3:
			return "EAC-3";
			
		case Codec::DTS:
			return "DTS";

		case Codec::DTSHDHR:
			return "DTS-HD HR";

		case Codec::SRT:
			return "SRT";
			
		case Codec::ASS:
			return "ASS";
	}

	return "Unknown";
}

TrackInfo::TrackInfo()
	: TrackInfo(Codec::None)
{
}

TrackInfo::TrackInfo(Codec codec_)
	: TrackInfo(codec_, std::string())
{
}

TrackInfo::TrackInfo(Codec codec_, std::string trackInfo_)
	: TrackInfo(codec_, trackInfo_, 0)
{
}


TrackInfo::TrackInfo(Codec codec_, std::string trackInfo_, int trackid_)
	: codec(codec_), trackInfo(trackInfo_), trackid(trackid_)
{
}

TrackInfo::Codec TrackInfo::GetCodec() const
{
	return codec;
}

int TrackInfo::GetTrackId() const
{
	return trackid;
}

void TrackInfo::WriteInfo(FILE* stream) const
{
	const std::string msg = [&]()
	{
		if(trackInfo.empty())
		{
			return (boost::format("Track: %1%"_gt) % FormatCodecTypeToString(codec)).str();
		}
		else
		{
			return (boost::format("Track: %1% (%2%)"_gt) % FormatCodecTypeToString(codec) % trackInfo).str();
		}
	}();
	fputs(msg.c_str(), stream);
	fputs("\n", stream);
}

}
}
