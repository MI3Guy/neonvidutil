/*
 * IOStreamBase.h
 *
 *  Created on: Mar 29, 2014
 *      Author: john
 */

#pragma once

#include <exception>

#ifdef _WIN32

#else
#include <sys/types.h>
#endif

namespace Neon {
namespace Core {

class IOStreamBase {
public:
	class FileOpenFailed : public std::exception {};
	class FileSeekFailed : public std::exception {};

	class SeekingNotSupported : public std::exception {};

#ifdef _WIN32
	using LengthType = std::int64_t;
#else
	using LengthType = off_t;
#endif

	enum class SeekType
	{
		Absolute,
		Relative,
		FromEnd
	};

	virtual bool CanSeek() const noexcept = 0;
	virtual LengthType Seek(LengthType pos) = 0;
	virtual LengthType Seek(SeekType type, LengthType pos) = 0;
	virtual LengthType Length() = 0;
	virtual LengthType Position() = 0;
	virtual void Close() = 0;


	virtual ~IOStreamBase()
	{
	}
};

} /* namespace Core */
} /* namespace Neon */

