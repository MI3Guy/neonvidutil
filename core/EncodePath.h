/*
 * EncodePath.h
 *
 *  Created on: Mar 29, 2014
 *      Author: john
 */

#pragma once

#include "core/FormatType.h"
#include "core/FormatHandler.h"
#include "core/FormatCodec.h"
#include "core/ConversionInfo.h"
#include "core/NeonContext.h"

#include <memory>
#include <vector>
#include <boost/optional.hpp>
#include <exception>


namespace Neon {
namespace Core {

class EncodePath final {
public:
	class NoEncodePathFound : public std::exception {};

	EncodePath(NeonContext& ctx_, FormatType input, FormatType output);
	~EncodePath();

private:
	NeonContext& ctx;
	std::vector<std::shared_ptr<FormatCodec>> steps;

	class EncodeNode
	{
	public:
		int Cost;
		FormatType Previous;
		boost::optional<ConversionInfo> Conversion;
	};

	static std::vector<std::shared_ptr<FormatCodec>> FindConversionPath(NeonContext& ctx, FormatType input, FormatType output);

public:
	void RunConversion(const std::string& inputFile, const std::string& outputFile);
	const std::vector<std::shared_ptr<FormatCodec>>& GetSteps() const;
};

} /* namespace Core */
} /* namespace Neon */

