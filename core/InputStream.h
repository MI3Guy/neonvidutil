#pragma once

#include "core/IOStreamBase.h"

#include <cstring>
#include <string>
#include <cstdio>
#include <memory>

namespace Neon
{
namespace Core
{

class OutputStream;

class InputStream : public IOStreamBase
{
public:
	class FileReadFailed : public std::exception {};

	virtual std::size_t Read(void* buffer, std::size_t size) = 0;

	virtual std::string GetAsFile();
	virtual void SeekForward(std::size_t num);

	void CopyTo(std::shared_ptr<OutputStream> other);
};

}
}

