/*
 * FileInputStream.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "FileInputStream.h"

#include <limits>

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#endif

namespace Neon {
namespace Core {

#ifdef _WIN32
FileInputStream::FileInputStream(const std::string& name)
{
	int bufferSize = MultiByteToWideChar(CP_UTF8, 0, name.c_str(), -1, NULL, 0);
	if(bufferSize == 0)
	{
		throw FileOpenFailed();
	}

	std::uint8_t buffer = new std::uint8_t[bufferSize];
	bufferSize = MultiByteToWideChar(CP_UTF8, 0, name.c_str(), -1, buffer, bufferSize);
	if(bufferSize == 0)
	{
		delete[] buffer;
		throw FileOpenFailed();
	}

	fh = CreateFileW(buffer, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0, nullptr);
	if(fh == INVALID_HANDLE_VALUE)
	{
		delete[] buffer;
		throw FileOpenFailed();
	}

	delete[] buffer;
}

FileInputStream::~FileInputStream() {
	if(fh = INVALID_HANDLE_VALUE)
	{
		CloseHandle(fh);
		fh = INVALID_HANDLE_VALUE;
	}
}

std::size_t FileInputStream::Read(void* buffer, std::size_t size)
{
	if(size > static_cast<std::size_t>(std::numeric_limits<ssize_t>::max()))
	{
		size = std::numeric_limits<ssize_t>::max();
	}

	ssize_t numBytesRead = read(fd, buffer, size);
	if(numBytesRead < 0)
	{
		throw FileReadFailed();
	}

	return static_cast<std::size_t>(numBytesRead);
}

bool FileInputStream::CanSeek() const noexcept
{
	return true;
}

FileInputStream::LengthType FileInputStream::Seek(LengthType pos)
{
	off_t ret = lseek(fd, pos, SEEK_SET);
	if(ret == -1)
	{
		throw FileSeekFailed();
	}

	return ret;
}

FileInputStream::LengthType FileInputStream::Seek(SeekType type, LengthType pos)
{
	int seekType = [&]()
	{
		switch(type)
		{
			case SeekType::FromEnd:
				return SEEK_END;

			case SeekType::Relative:
				return SEEK_CUR;

			case SeekType::Absolute:
			default:
				return SEEK_SET;
		}
	}();

	off_t ret = lseek(fd, pos, seekType);
	if(ret == -1)
	{
		throw FileSeekFailed();
	}

	return ret;
}

void FileInputStream::SeekForward(std::size_t num)
{
	static_assert(sizeof(off_t) >= sizeof(std::size_t), "size_t is larger than off_t");


	if((sizeof(off_t) <= sizeof(std::size_t)) && num > static_cast<std::size_t>(std::numeric_limits<off_t>::max()))
	{
		throw FileSeekFailed();
	}

	if(lseek(fd, static_cast<off_t>(num), SEEK_CUR) == -1)
	{
		throw FileSeekFailed();
	}
}

FileInputStream::LengthType FileInputStream::Length()
{
	struct stat statInfo;
	if(fstat(fd, &statInfo) == -1)
	{
		throw FileSeekFailed();
	}

	return statInfo.st_size;
}

FileInputStream::LengthType FileInputStream::Position()
{
	off_t ret = lseek(fd, 0, SEEK_CUR);
	if(ret == -1)
	{
		throw FileSeekFailed();
	}

	return ret;
}

#else

FileInputStream::FileInputStream(const std::string& name)
	: FileInputStream(open(name.c_str(), O_RDONLY | O_CLOEXEC))
{
}

FileInputStream::FileInputStream(int fd_)
	: fd(fd_)
{
	if(fd == -1)
	{
		throw FileOpenFailed();
	}
}

FileInputStream::~FileInputStream() {
	if(fd != -1)
	{
		close(fd);
		fd = -1;
	}
}

std::size_t FileInputStream::Read(void* buffer, std::size_t size)
{
	if(size > static_cast<std::size_t>(std::numeric_limits<ssize_t>::max()))
	{
		size = std::numeric_limits<ssize_t>::max();
	}

	ssize_t numBytesRead = read(fd, buffer, size);
	if(numBytesRead < 0)
	{
		throw FileReadFailed();
	}

	return static_cast<std::size_t>(numBytesRead);
}

bool FileInputStream::CanSeek() const noexcept
{
	return true;
}

FileInputStream::LengthType FileInputStream::Seek(LengthType pos)
{
	off_t ret = lseek(fd, pos, SEEK_SET);
	if(ret == -1)
	{
		throw FileSeekFailed();
	}

	return ret;
}

FileInputStream::LengthType FileInputStream::Seek(SeekType type, LengthType pos)
{
	int seekType = [&]()
	{
		switch(type)
		{
			case SeekType::FromEnd:
				return SEEK_END;

			case SeekType::Relative:
				return SEEK_CUR;

			case SeekType::Absolute:
			default:
				return SEEK_SET;
		}
	}();

	off_t ret = lseek(fd, pos, seekType);
	if(ret == -1)
	{
		throw FileSeekFailed();
	}

	return ret;
}

void FileInputStream::SeekForward(std::size_t num)
{
	static_assert(sizeof(off_t) >= sizeof(std::size_t), "size_t is larger than off_t");


	if((sizeof(off_t) <= sizeof(std::size_t)) && num > static_cast<std::size_t>(std::numeric_limits<off_t>::max()))
	{
		throw FileSeekFailed();
	}

	if(lseek(fd, static_cast<off_t>(num), SEEK_CUR) == -1)
	{
		throw FileSeekFailed();
	}
}

FileInputStream::LengthType FileInputStream::Length()
{
	struct stat statInfo;
	if(fstat(fd, &statInfo) == -1)
	{
		throw FileSeekFailed();
	}

	return statInfo.st_size;
}

FileInputStream::LengthType FileInputStream::Position()
{
	off_t ret = lseek(fd, 0, SEEK_CUR);
	if(ret == -1)
	{
		throw FileSeekFailed();
	}

	return ret;
}

void FileInputStream::Close()
{
	if(fd != -1)
	{
		if(close(fd) < 0)
		{
			throw FileReadFailed();
		}
		fd = -1;
	}
}

#endif

} /* namespace Core */
} /* namespace Neon */
