#pragma once

#include <string>
#include <iostream>

namespace Neon
{
namespace Core
{

class TrackInfo
{
public:
	
	enum class Codec
	{
		Unknown,
		
		None,
		
		// Video
		AVC,
		VC1,
		MPEG2Video,
		
		// Audio
		PCM,
		FLAC,
		WavPack,
		TrueHD,
		DTSHDMA,
		AC3,
		EAC3,
		DTS,
		DTSHDHR,
		
		// Subtitles
		SRT,
		ASS
	};
	
	static std::string FormatCodecTypeToString(Codec codec);
		
	TrackInfo();
	TrackInfo(Codec codec);
	TrackInfo(Codec codec, std::string trackInfo_);
	TrackInfo(Codec codec, std::string trackInfo_, int trackid);
	
private:
	Codec codec;
	std::string trackInfo;
	int trackid;
	
public:
	Codec GetCodec() const;
	std::string GetTrackInfo() const;
	int GetTrackId() const;

	void WriteInfo(FILE* stream) const;

		
};
	
}
}

