/*
 * FileOutputStream.cpp
 *
 *  Created on: Mar 29, 2014
 *      Author: john
 */

#include "FileOutputStream.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits>
#include <unistd.h>

namespace Neon {
namespace Core {

FileOutputStream::FileOutputStream(const std::string& name)
	: fd(open(name.c_str(), O_WRONLY | O_TRUNC | O_CREAT | O_CLOEXEC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH))
{
	if(fd == -1)
	{
		throw FileOpenFailed();
	}
}

FileOutputStream::~FileOutputStream() {
	if(fd != -1)
	{
		close(fd);
		fd = -1;
	}
}

void FileOutputStream::Write(const void* buffer, std::size_t size)
{
	const std::uint8_t* buffer2 = static_cast<const std::uint8_t*>(buffer);

	while(size > 0)
	{
		ssize_t numBytes = write(fd, buffer2, size);
		if(numBytes != -1)
		{
			buffer2 += numBytes;
			size -= numBytes;
		}
		else
		{
			if(!(errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR))
			{
				throw FileWriteFailed();
			}
		}
	}
}

bool FileOutputStream::CanSeek() const noexcept
{
	return true;
}

FileOutputStream::LengthType FileOutputStream::Seek(LengthType pos)
{
	off_t ret = lseek(fd, pos, SEEK_SET);
	if(ret == -1)
	{
		throw FileSeekFailed();
	}

	return ret;
}

FileOutputStream::LengthType FileOutputStream::Seek(SeekType type, LengthType pos)
{
	int seekType = [&]()
	{
		switch(type)
		{
			case SeekType::FromEnd:
				return SEEK_END;

			case SeekType::Relative:
				return SEEK_CUR;

			case SeekType::Absolute:
			default:
				return SEEK_SET;
		}
	}();

	off_t ret = lseek(fd, pos, seekType);
	if(ret == -1)
	{
		throw FileSeekFailed();
	}

	return ret;
}

FileOutputStream::LengthType FileOutputStream::Length()
{
	struct stat statInfo;
	if(fstat(fd, &statInfo) == -1)
	{
		throw FileSeekFailed();
	}

	return statInfo.st_size;
}

FileOutputStream::LengthType FileOutputStream::Position()
{
	off_t ret = lseek(fd, 0, SEEK_CUR);
	if(ret == -1)
	{
		throw FileSeekFailed();
	}

	return ret;
}

void FileOutputStream::Close()
{
	if(fd != -1)
	{
		if(close(fd) < 0)
		{
			throw FileWriteFailed();
		}
		fd = -1;
	}
}


} /* namespace Core */
} /* namespace Neon */
