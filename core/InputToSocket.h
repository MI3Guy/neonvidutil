/*
 * InputToSocket.h
 *
 *  Created on: Apr 16, 2014
 *      Author: john
 */

#pragma once

#include "core/InputStream.h"

#include <thread>
#include <exception>
#include <boost/optional.hpp>
#include <cstdint>
#include <memory>

namespace Neon {
namespace Core {

class InputToSocket final {
public:
	InputToSocket(std::shared_ptr<InputStream> stream_, const void* buffer, std::size_t bufferSize);
	virtual ~InputToSocket();

	class SocketCreationFailed : public std::exception {};
	class SocketIOFailed : public std::exception {};

private:
	std::uint16_t port;
	boost::optional<std::exception> error;
	std::thread thread;
	std::shared_ptr<InputStream> stream;

public:

	std::uint16_t GetPort() const;

	void WaitForStreamEnd();

};

} /* namespace Core */
} /* namespace Neon */

