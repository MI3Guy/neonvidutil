/*
 * BinaryReader.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "BinaryReader.h"


namespace Neon {
namespace Core {

BinaryReaderNative::BinaryReaderNative(InputStream& stream_)
	: stream(stream_)
{
}

BinaryReaderNative::~BinaryReaderNative() {
}

std::uint32_t BinaryReaderNative::ReadUInt32()
{
	std::uint32_t buffer;
	ReadBytes(&buffer, sizeof(buffer));
	return buffer;
}

std::uint16_t BinaryReaderNative::ReadUInt16()
{
	std::uint16_t buffer;
	ReadBytes(&buffer, sizeof(buffer));
	return buffer;
}

void BinaryReaderNative::ReadBytes(void* buffer, size_t num)
{
	while(num > 0)
	{
		size_t readBytes = stream.Read(buffer, num);
		if(readBytes == 0)
		{
			throw EndOfFileException();
		}

		if(readBytes > num)
		{
			throw ReadTooMuchException();
		}

		num -= readBytes;
		buffer = static_cast<std::uint8_t*>(buffer) + readBytes;
	}
}

void BinaryReaderNative::SkipBytes(std::size_t num)
{
	stream.SeekForward(num);
}

} /* namespace Core */
} /* namespace Neon */
