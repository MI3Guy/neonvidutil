/*
 * InputToSocket.cpp
 *
 *  Created on: Apr 16, 2014
 *      Author: john
 */

#include <core/InputToSocket.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

namespace Neon {
namespace Core {

InputToSocket::InputToSocket(std::shared_ptr<InputStream> stream_, const void* buffer, std::size_t bufferSize)
	: stream(stream_)
{
	int socket = ::socket(AF_INET, SOCK_STREAM, 0);

	if(socket < 0)
	{
		throw SocketCreationFailed();
	}

	sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = 0;

	if(bind(socket, reinterpret_cast<sockaddr*>(&addr), sizeof(addr)) < 0)
	{
		throw SocketCreationFailed();
	}

	socklen_t len = sizeof(addr);
	if(getsockname(socket, reinterpret_cast<sockaddr*>(&addr), &len) == -1)
	{
		throw SocketCreationFailed();
	}

	port = ntohs(addr.sin_port);

	thread = std::thread([this, socket, buffer, bufferSize]
	{
		try
		{
			listen(socket, 1);

			sockaddr_in clientAddr;
			socklen_t clientLength;

			int writeSocketId = accept(socket, reinterpret_cast<sockaddr*>(&clientAddr), &clientLength);
			if(writeSocketId < 0)
			{
				throw SocketIOFailed();
			}

			if(write(writeSocketId, buffer, bufferSize) < 0)
			{
				throw SocketIOFailed();
			}

			std::uint8_t buffer[0x1000];
			while(std::size_t numRead = stream->Read(buffer, sizeof(buffer)))
			{
				if(write(writeSocketId, buffer, numRead) < 0)
				{
					throw SocketIOFailed();
				}
			}

			if(close(writeSocketId) < 0)
			{
				throw SocketIOFailed();
			}
			close(socket);
		}
		catch(std::exception& ex)
		{
			error = ex;
		}
		catch(...)
		{
			error = std::exception();
		}
	});
}

InputToSocket::~InputToSocket()
{
}

std::uint16_t InputToSocket::GetPort() const
{
	return port;
}

void InputToSocket::WaitForStreamEnd()
{
	thread.join();
	if(error)
	{
		throw *error;
	}
}

} /* namespace Core */
} /* namespace Neon */
