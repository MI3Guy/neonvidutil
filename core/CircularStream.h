/*
 * CircularStream.h
 *
 *  Created on: Mar 30, 2014
 *      Author: john
 */

#pragma once

#include "core/InputStream.h"
#include "core/OutputStream.h"

#include <cstdint>
#include <atomic>
#include <condition_variable>
#include <exception>
#include <memory>

namespace Neon {
namespace Core {

class CircularStream final : public std::enable_shared_from_this<CircularStream> {
public:
	class StreamClosed : public std::exception {};

	CircularStream();
	~CircularStream();

	std::shared_ptr<InputStream> CreateInputStream();
	std::shared_ptr<OutputStream> CreateOutputStream();

private:
	static constexpr std::size_t numBuffers = 20;
	static constexpr std::size_t bufferSize = 0x1000;
	std::uint8_t dataBuffers[numBuffers][bufferSize];

	std::size_t readBufferIndex;
	std::size_t writeBufferIndex;
	std::size_t numFullBuffers;

	std::size_t readBufferPosition;
	std::size_t writeBufferPosition;

	std::mutex syncLock;
	std::condition_variable bufferEmptied;
	std::condition_variable bufferFilled;


	bool finished;

	void WriteData(const void* buffer, std::size_t size);
	std::size_t ReadData(void* buffer, std::size_t size);
	void DoneWriting();

	class Reader final : public InputStream
	{
	public:
		Reader(std::shared_ptr<CircularStream> circular);

	private:
		std::shared_ptr<CircularStream> circularStream;

	public:
		virtual std::size_t Read(void* buffer, std::size_t size) override;
		virtual bool CanSeek() const noexcept override;
		virtual LengthType Seek(LengthType pos) override;
		virtual LengthType Seek(SeekType type, LengthType pos) override;
		virtual LengthType Length() override;
		virtual LengthType Position() override;
		virtual void Close() override;
	};

	class Writer final : public OutputStream
	{
	public:
		Writer(std::shared_ptr<CircularStream> circular);
		~Writer();

	private:
		std::shared_ptr<CircularStream> circularStream;

	public:
		virtual void Write(const void* buffer, std::size_t size) override;
		virtual bool CanSeek() const noexcept override;
		virtual LengthType Seek(LengthType pos) override;
		virtual LengthType Seek(SeekType type, LengthType pos) override;
		virtual LengthType Length() override;
		virtual LengthType Position() override;
		virtual void Close() override;
	};
};

} /* namespace Core */
} /* namespace Neon */
