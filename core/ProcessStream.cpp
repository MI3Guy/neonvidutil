/*
 * ProcessStream.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "ProcessStream.h"
#include "Process.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <limits>
#include <unistd.h>
#include <cstring>

namespace Neon {
namespace Core {

constexpr const char* ProcessStream::StandardInputArgument;
constexpr const char* ProcessStream::StandardOutputArgument;


ProcessStream::ProcessStream(int redirectMode, const std::string& program, std::vector<std::string> arguments)
{
	bool handleInput = redirectMode & RedirectStandardInput;
	bool handleOutput = redirectMode & RedirectStandardOutput;
	bool handleError = redirectMode & RedirectStandardError;

	int inpipe[2] = { -1, -1 };
	int outpipe[2] = { -1, -1 };
	int errpipe[2] = { -1, -1 };

	if((handleInput && pipe(inpipe) < 0) || (handleOutput && pipe(outpipe) < 0) || (handleError && pipe(errpipe) < 0))
	{
		throw IOStreamBase::FileOpenFailed();
	}

	pid_t pid = fork();
	if(pid < 0)
	{
		throw IOStreamBase::FileOpenFailed();
	}
	else if(pid == 0)
	{
		if(handleInput)
		{
			close(inpipe[1]);
			dup2(inpipe[0], STDIN_FILENO);
		}
		if(handleOutput)
		{
			close(outpipe[0]);
			dup2(outpipe[1], STDOUT_FILENO);
		}
		if(handleError)
		{
			close(errpipe[0]);
			dup2(errpipe[1], STDERR_FILENO);
		}

		char** args = new char*[arguments.size() + 2];
		args[0] = strdup(program.c_str());
		for(std::size_t i = 0; i < arguments.size(); ++i)
		{
			args[i + 1] = strdup(arguments[i].c_str());
		}

		args[arguments.size() + 1] = nullptr;

		execvp(program.c_str(), args);
		exit(1);
	}

	if(handleInput)
	{
		close(inpipe[0]);
	}
	if(handleOutput)
	{
		close(outpipe[1]);
	}

	readHandle = outpipe[0];
	writeHandle = inpipe[1];
	errorHandle = errpipe[0];
	this->pid = pid;
}

ProcessStream::~ProcessStream()
{
	if(errorHandle != -1)
	{
		close(errorHandle);
		errorHandle = -1;
	}
}

boost::optional<int> ProcessStream::Wait()
{
	int ret;
	if(waitpid(pid, &ret, 0) < 1)
	{
		throw Process::ProcessWaitError();
	}

	if(WIFEXITED(ret))
	{
		return WEXITSTATUS(ret);
	}
	else
	{
		return boost::optional<int>();
	}
}

std::shared_ptr<InputStream> ProcessStream::CreateErrorInputStream()
{
	std::shared_ptr<InputStream> ret(new PipeInputStream(errorHandle));
	errorHandle = -1;
	return ret;
}


} /* namespace Core */
} /* namespace Neon */
