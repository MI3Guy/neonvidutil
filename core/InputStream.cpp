#include "core/InputStream.h"
#include "core/OutputStream.h"

#include <cstdio>

namespace Neon
{
namespace Core
{

void InputStream::SeekForward(std::size_t num)
{
	std::uint8_t buffer[0x1000];
	while(num > 0)
	{
		std::size_t numToRead = std::min(num, sizeof(buffer));

		std::size_t numRead = Read(buffer, numToRead);
		if(numRead == 0)
		{
			return;
		}

		num -= numRead;
	}
}

std::string InputStream::GetAsFile()
{
	std::uint8_t buffer[0x1000];
	char filename[L_tmpnam];
	tmpnam(filename);
	
	FILE* fp = fopen(filename, "wb");
	std::size_t num;
	
	while((num = Read(buffer, sizeof(buffer))) != 0)
	{
		fwrite(buffer, 1, num, fp);
	}
	
	fclose(fp);
	
	return filename;
}

void InputStream::CopyTo(std::shared_ptr<Core::OutputStream> other)
{
	std::uint8_t buffer[0x1000];
	while(std::size_t numBytes = Read(buffer, sizeof(buffer)))
	{
		other->Write(buffer, numBytes);
	}
}

}
}

