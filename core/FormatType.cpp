#include "core/FormatType.h"

#include "core/lang.h"

namespace Neon
{
namespace Core
{

std::string FormatType::FormatContainerToString(Container container)
{
	switch(container)
	{
		case Container::Unknown:
			return "Unknown"_gt;
			
		case Container::None:
			return "None"_gt;
			
		case Container::Matroska:
			return "Matroska";
			
		case Container::VC1:
			return "VC-1";
			
		case Container::MPEGVideo:
			return "MPEG";
			
		case Container::Wave:
			return "Wave";
			
		case Container::FLAC:
			return "FLAC";
			
		case Container::WavPack:
			return "WavPack";
			
		case Container::TrueHD:
			return "TrueHD";
			
		case Container::AC3:
			return "AC-3";
			
		case Container::EAC3:
			return "EAC-3";
			
		case Container::DTS:
			return "DTS";
			
		default:
			return "Unknown"_gt;
	}
}

FormatType::FormatType()
	: FormatType(Container::None)
{
}

FormatType::FormatType(Container container_)
	: FormatType(container_, std::vector<TrackInfo>())
{
}

FormatType::FormatType(Container container_, std::vector<TrackInfo> tracks_)
	: container(container_), tracks(tracks_)
{
}

FormatType FormatType::Simple(Container container, TrackInfo::Codec codec)
{
	return FormatType(container, { TrackInfo(codec) });
}

FormatType::Container FormatType::GetContainer() const
{
	return container;
}

const std::vector<TrackInfo>& FormatType::GetTracks() const
{
	return tracks;
}


void FormatType::WriteInfo(FILE* stream) const
{
	fprintf(stream, "Container Info: %s\n"_gt, FormatContainerToString(container).c_str());
	fprintf(stream, Translate("\tContains %zu track\n", "\tContains %zu tracks\n", tracks.size()), tracks.size());

	for(const auto& track : tracks)
	{
		track.WriteInfo(stream);
	}
}

bool FormatType::operator <(const FormatType& other) const
{
	if(container < other.container)
	{
		return true;
	}
	else if(other.container < container)
	{
		return false;
	}

	if(tracks.size() < other.tracks.size())
	{
		return true;
	}
	else if(other.tracks.size() < tracks.size())
	{
		other.tracks.size();
	}

	for(auto myTrackIter = tracks.begin(), otherTrackIter = other.tracks.begin(); myTrackIter != tracks.end() && otherTrackIter != other.tracks.end(); ++myTrackIter, ++otherTrackIter)
	{
		if(myTrackIter->GetCodec() < otherTrackIter->GetCodec())
		{
			return true;
		}
		else if(otherTrackIter->GetCodec() < myTrackIter->GetCodec())
		{
			return false;
		}
	}

	return false;

}

}
}
