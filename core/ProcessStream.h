/*
 * ProcessStream.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#pragma once

#include "core/InputStream.h"
#include "core/OutputStream.h"
#include "core/PipeStream.h"

#include <exception>
#include <unistd.h>
#include <string.h>
#include <memory>
#include <string>
#include <vector>
#include <boost/optional.hpp>

namespace Neon {
namespace Core {

class ProcessStream final : public PipeStream {

public:
	static const int RedirectStandardInput = 0x01;
	static const int RedirectStandardOutput = 0x02;
	static const int RedirectStandardError = 0x04;

	static constexpr const char* StandardInputArgument = "/dev/stdin";
	static constexpr const char* StandardOutputArgument = "/dev/stdout";

	ProcessStream(int redirectMode, const std::string& program, std::vector<std::string> arguments);

	// Three different overloads are used here to prevent the compiler from performing infinite recursion.
	ProcessStream(int redirectMode, const std::string program)
		: ProcessStream(redirectMode, program, std::vector<std::string> {})
	{
	}

	ProcessStream(int redirectMode, const std::string program, const std::string& arg)
		: ProcessStream(redirectMode, program, std::vector<std::string> { arg })
	{
	}

	template<typename... Args>
	ProcessStream(int redirectMode, const std::string program, const std::string& arg, Args&&... args)
		: ProcessStream(redirectMode, program, std::vector<std::string> { arg, args... })
	{
	}

	~ProcessStream();

	boost::optional<int> Wait();

	std::shared_ptr<Core::InputStream> CreateErrorInputStream();

protected:
#ifdef _WIN32
#else
	pid_t pid;
	int errorHandle;
#endif
};

} /* namespace Core */
} /* namespace Neon */
