/*
 * BinaryReader.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#pragma once

#include "core/InputStream.h"
#include <cinttypes>
#include <exception>

#include <boost/detail/endian.hpp>

namespace Neon {
namespace Core {

class BinaryReaderNative {
public:
	class EndOfFileException : public std::exception {};
	class ReadTooMuchException : public std::exception {};

	BinaryReaderNative(InputStream& stream);
	virtual ~BinaryReaderNative();

private:
	InputStream& stream;

public:
	InputStream& GetInputStream()
	{
		return stream;
	}


	std::uint32_t ReadUInt32();
	std::uint16_t ReadUInt16();
	void ReadBytes(void* buffer, size_t num);

	void SkipBytes(size_t num);
};

#ifdef BOOST_LITTLE_ENDIAN
using BinaryReaderLE = BinaryReaderNative;
#else
using BinaryReaderBE = BinaryReaderNative;
#endif

} /* namespace Core */
} /* namespace Neon */

