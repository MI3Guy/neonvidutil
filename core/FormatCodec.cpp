/*
 * FormatCodec.cpp
 *
 *  Created on: Mar 15, 2014
 *      Author: john
 */

#include "FormatCodec.h"
#include "core/FileOutputStream.h"
#include "core/PipeStream.h"

namespace Neon {
namespace Core {

FormatCodec::FormatCodec() {
}

FormatCodec::~FormatCodec() {
}

IOStreamPair FormatCodec::InitConvertData(std::shared_ptr<InputStream> inStream)
{
	std::shared_ptr<Core::PipeStream> circular(new Core::PipeStream());

	auto inputStream = circular->CreateInputStream();
	auto outputStream = circular->CreateOutputStream();

	return Core::IOStreamPair(inputStream, outputStream);
}

std::shared_ptr<OutputStream> FormatCodec::InitConvertData(std::shared_ptr<InputStream> inStream, const std::string& file)
{
	return std::shared_ptr<Core::OutputStream>(new Core::FileOutputStream(file));
}


} /* namespace Core */
} /* namespace Neon */
