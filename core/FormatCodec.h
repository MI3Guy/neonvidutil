/*
 * FormatCodec.h
 *
 *  Created on: Mar 15, 2014
 *      Author: john
 */

#pragma once

#include "core/InputStream.h"
#include "core/OutputStream.h"
#include "core/IOStreamPair.h"
#include <string>
#include <memory>
#include <exception>

namespace Neon {
namespace Core {

class FormatCodec {
public:
	class ConversionError : public std::exception
	{
	public:
		ConversionError(std::string msg)
			: message(msg)
		{
		}

		virtual const char* what() const noexcept(true) override
		{
			return message.c_str();
		}

	private:
		std::string message;
	};

	FormatCodec();
	virtual ~FormatCodec();

	virtual std::string GetName() const = 0;

	virtual IOStreamPair InitConvertData(std::shared_ptr<InputStream> inStream);
	virtual std::shared_ptr<OutputStream> InitConvertData(std::shared_ptr<InputStream> inStream, const std::string& file);

	virtual void ConvertData(IOStreamPair streams) = 0;

};

} /* namespace Core */
} /* namespace Neon */

