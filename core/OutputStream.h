/*
 * OutputStream.h
 *
 *  Created on: Mar 29, 2014
 *      Author: john
 */

#pragma once

#include "core/IOStreamBase.h"

#include <cstring>

namespace Neon {
namespace Core {

class OutputStream : public IOStreamBase
{
public:
	class FileWriteFailed : public std::exception {};

	virtual void Write(const void* buffer, std::size_t size) = 0;
};

} /* namespace Core */
} /* namespace Neon */
