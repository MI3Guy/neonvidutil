/*
 * Process.h
 *
 *  Created on: Apr 16, 2014
 *      Author: john
 */

#pragma once

#include <string>
#include <vector>
#include <exception>

#include <boost/optional.hpp>

#ifdef _WIN32
#else
#include <unistd.h>
#endif

namespace Neon {
namespace Core {

class Process
{
public:
	Process(const std::string& program, const std::vector<std::string>& arguments);

	// Three different overloads are used here to prevent the compiler from performing infinite recursion.
	Process(const std::string program)
		: Process(program, std::vector<std::string> {})
	{
	}

	Process(const std::string program, const std::string& arg)
		: Process(program, std::vector<std::string> { arg })
	{
	}

	template<typename... Args>
	Process(const std::string program, const std::string& arg, Args&&... args)
		: Process(program, std::vector<std::string> { arg, args... })
	{
	}

	~Process();

	class ProcessStartError : public std::exception {};
	class ProcessWaitError : public std::exception {};

private:
#ifdef _WIN32
#else
	pid_t pid;
#endif

public:
	boost::optional<int> Wait();
};

} /* namespace Core */
} /* namespace Neon */
