/*
 * SocketInputStream.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "SocketInputStream.h"

#include <limits>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

namespace Neon {
namespace Core {

SocketInputStream::SocketInputStream()
	: socketfd(socket(AF_INET, SOCK_STREAM, 0)),
	  connfd(-1)
{
	if(socketfd < 0)
	{
		throw FileOpenFailed();
	}

	sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = 0;

	if(bind(socketfd, reinterpret_cast<sockaddr*>(&addr), sizeof(addr)) < 0)
	{
		throw FileOpenFailed();
	}

	socklen_t len = sizeof(addr);
	if(getsockname(socketfd, reinterpret_cast<sockaddr*>(&addr), &len) == -1)
	{
		throw FileOpenFailed();
	}

	port = ntohs(addr.sin_port);

	connectWaitThread = std::thread([this]
	{
		listen(socketfd, 1);

		sockaddr_in clientAddr;
		socklen_t clientLength;

		connfd = accept(socketfd, reinterpret_cast<sockaddr*>(&clientAddr), &clientLength);
		if(connfd < 0)
		{
			throw FileReadFailed();
		}
	});
}

SocketInputStream::~SocketInputStream()
{
	if(socketfd != -1)
	{
		close(socketfd);
		socketfd = -1;
	}
	if(connfd != -1)
	{
		close(connfd);
		connfd = -1;
	}
}

std::size_t SocketInputStream::Read(void* buffer, std::size_t size)
{
	if(connectWaitThread.joinable())
	{
		connectWaitThread.join();
	}

	if(size > static_cast<std::size_t>(std::numeric_limits<ssize_t>::max()))
	{
		size = std::numeric_limits<ssize_t>::max();
	}

	ssize_t numBytesRead = read(connfd, buffer, size);
	if(numBytesRead < 0)
	{
		throw FileReadFailed();
	}

	return static_cast<std::size_t>(numBytesRead);
}

bool SocketInputStream::CanSeek() const noexcept
{
	return false;
}

SocketInputStream::LengthType SocketInputStream::Seek(LengthType pos)
{
	throw SeekingNotSupported();
}

SocketInputStream::LengthType SocketInputStream::Seek(SeekType type, LengthType pos)
{
	throw SeekingNotSupported();
}

SocketInputStream::LengthType SocketInputStream::Length()
{
	throw SeekingNotSupported();
}

SocketInputStream::LengthType SocketInputStream::Position()
{
	throw SeekingNotSupported();
}

void SocketInputStream::Close()
{
	if(connfd != -1)
	{
		if(close(connfd) < 0)
		{
			throw FileReadFailed();
		}
		connfd = -1;
	}
	close(socketfd);
}


std::uint16_t SocketInputStream::GetPort() const
{
	return port;
}

} /* namespace Core */
} /* namespace Neon */
