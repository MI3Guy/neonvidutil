/*
 * FormatHandler.cpp
 *
 *  Created on: Mar 15, 2014
 *      Author: john
 */

#include "FormatHandler.h"
#include <stdexcept>

namespace Neon {
namespace Core {

FormatHandler::FormatHandler() {
	// TODO Auto-generated constructor stub

}

FormatHandler::~FormatHandler() {
	// TODO Auto-generated destructor stub
}

std::shared_ptr<InputStream> FormatHandler::OpenInput(const std::string& path) const
{
	return std::shared_ptr<InputStream>();
}

std::vector<std::tuple<std::string, FormatType>> FormatHandler::IdentifyInput(const std::string& path) const
{
	return std::vector<std::tuple<std::string, FormatType>>();
}

boost::optional<FormatType> FormatHandler::IdentifyOutput(const std::string& path) const
{
#ifdef _WIN32

#else
	std::size_t dotIndex = path.find_last_of(".");
	std::size_t slashIndex = path.find_last_of("/");

	if(slashIndex != std::string::npos && (dotIndex == std::string::npos || dotIndex <= slashIndex + 1))
	{
		return boost::optional<FormatType>();
	}

	std::string ext = path.substr(dotIndex + 1);
#endif

	auto outputFormatIter = outputFormats.find(ext);
	if(outputFormatIter != outputFormats.end())
	{
		return outputFormatIter->second;
	}
	else
	{
		return boost::optional<FormatType>();
	}

}

std::vector<ConversionInfo> FormatHandler::GetConversions(const FormatType& source) const
{
	return std::vector<ConversionInfo>();
}

std::shared_ptr<FormatCodec> FormatHandler::ConvertStream(const ConversionInfo& info) const
{
	throw UnsupportedConversion();
}

} /* namespace Core */
} /* namespace Neon */
