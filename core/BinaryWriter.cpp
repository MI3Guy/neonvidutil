/*
 * BinaryWriterNative.cpp
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#include "BinaryWriter.h"

namespace Neon {
namespace Core {

BinaryWriterNative::BinaryWriterNative(OutputStream& stream_)
	: stream(stream_)
{
}

BinaryWriterNative::~BinaryWriterNative() {
}

void WriteUInt32(std::uint32_t value);
void WriteUInt16(std::uint16_t value);

void BinaryWriterNative::WriteUInt32(std::uint32_t value)
{
	stream.Write(&value, sizeof(std::uint32_t));
}

void BinaryWriterNative::WriteUInt16(std::uint16_t value)
{
	stream.Write(&value, sizeof(std::uint16_t));
}


} /* namespace Core */
} /* namespace Neon */
