#pragma once

#include "core/TrackInfo.h"

#include <string>
#include <vector>
#include <iostream>

namespace Neon
{
namespace Core
{

class FormatType
{
public:
	enum class Container
	{
		/// <summary>
		/// Unknown is an invalid container type.
		/// </summary>
		Unknown,
	
		/// <summary>
		/// Implies that the data is a raw stream.
		/// </summary>
		None,
	
		Matroska,
	
		VC1,
		MPEGVideo,
	
		Wave,
		FLAC,
		WavPack,
		TrueHD,
		AC3,
		EAC3,
		DTS
	
	};
	
	static std::string FormatContainerToString(Container container);
		
	FormatType();
	FormatType(Container container);
	FormatType(Container container, std::vector<TrackInfo> tracks);
	

	static FormatType Simple(Container container, TrackInfo::Codec codec);

	Container container;
	std::vector<TrackInfo> tracks;
	
	Container GetContainer() const;
	const std::vector<TrackInfo>& GetTracks() const;

	void WriteInfo(FILE* stream) const;

	bool operator<(const FormatType& other) const;
};
	
}
}

