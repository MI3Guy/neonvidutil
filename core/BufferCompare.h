/*
 * BufferCompare.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#pragma once

#include <cstdint>

namespace Neon {
namespace Core {

bool BufferEquals(const void* buff1, const void* buff2, std::size_t length);

} /* namespace Core */
} /* namespace Neon */

