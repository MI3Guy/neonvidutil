/*
 * IOStreamPair.h
 *
 *  Created on: Mar 31, 2014
 *      Author: john
 */

#pragma once

#include "core/InputStream.h"
#include "core/OutputStream.h"

#include <memory>

namespace Neon {
namespace Core {

class IOStreamPair final
{
public:
	IOStreamPair(std::shared_ptr<InputStream> inputStream, std::shared_ptr<OutputStream> outputStream);

	const std::shared_ptr<InputStream> Input;
	const std::shared_ptr<OutputStream> Output;
};

} /* namespace Core */
} /* namespace Neon */
