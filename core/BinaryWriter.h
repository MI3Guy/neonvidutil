/*
 * BinaryWriter.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#pragma once

#include "core/OutputStream.h"
#include <cinttypes>
#include <exception>
#include <boost/detail/endian.hpp>

namespace Neon {
namespace Core {

class BinaryWriterNative {
public:
	BinaryWriterNative(OutputStream& stream);
	virtual ~BinaryWriterNative();

private:
	OutputStream& stream;

public:
	OutputStream& GetOutputStream()
	{
		return stream;
	}


	void WriteUInt32(std::uint32_t value);
	void WriteUInt16(std::uint16_t value);
};

#ifdef BOOST_LITTLE_ENDIAN
using BinaryWriterLE = BinaryWriterNative;
#else
using BinaryWriterBE = BinaryWriterNative;
#endif

} /* namespace Core */
} /* namespace Neon */

