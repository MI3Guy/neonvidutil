/*
 * NeonContext.cpp
 *
 *  Created on: Mar 21, 2014
 *      Author: john
 */

#include "NeonContext.h"

namespace Neon {
namespace Core {

NeonContext::NeonContext()
	: plugins(FormatHandlerFactoryBase::CreateHandlers())
{
}

NeonContext::~NeonContext() {
}

std::shared_ptr<InputStream> NeonContext::OpenInput(const std::string& file) const
{
	for(auto plugin : plugins)
	{
		auto ret = plugin->OpenInput(file);
		if(ret)
		{
			return ret;
		}
	}

	return std::shared_ptr<InputStream>();
}

std::vector<std::tuple<std::string, FormatType>> NeonContext::IdentifyInputFile(const std::string& file) const
{
	for(auto plugin : plugins)
	{
		std::vector<std::tuple<std::string, FormatType>> ret = plugin->IdentifyInput(file);
		if(!ret.empty())
		{
			return ret;
		}
	}

	return std::vector<std::tuple<std::string, FormatType>>();
}

boost::optional<FormatType> NeonContext::IdentifyOutputFile(const std::string& file) const
{
	for(auto plugin : plugins)
	{
		boost::optional<FormatType> ret = plugin->IdentifyOutput(file);
		if(ret)
		{
			return ret;
		}
	}

	return boost::optional<FormatType>();
}

std::vector<ConversionInfo> NeonContext::FindConversionTypes(const FormatType& source) const
{
	std::vector<ConversionInfo> ret;
	for(auto plugin : plugins)
	{
		std::vector<ConversionInfo> convs = plugin->GetConversions(source);
		for(ConversionInfo& conv : convs)
		{
			ret.push_back(conv);
		}
	}
	return ret;
}

} /* namespace Core */
} /* namespace Neon */
