/*
 * Process.cpp
 *
 *  Created on: Apr 16, 2014
 *      Author: john
 */

#include <core/Process.h>

#include <cstring>

#include <sys/wait.h>

namespace Neon {
namespace Core {

Process::Process(const std::string& program, const std::vector<std::string>& arguments) {
	pid_t pid = fork();
	if(pid < 0)
	{
		throw ProcessStartError();
	}
	else if(pid == 0)
	{
		char** args = new char*[arguments.size() + 2];
		args[0] = strdup(program.c_str());
		for(std::size_t i = 0; i < arguments.size(); ++i)
		{
			args[i + 1] = strdup(arguments[i].c_str());
		}

		args[arguments.size() + 1] = nullptr;

		execvp(program.c_str(), args);
		exit(1);
	}
	else
	{
		this->pid = pid;
	}
}

Process::~Process() {
}

boost::optional<int> Process::Wait()
{
	int ret;
	if(waitpid(pid, &ret, 0) < 1)
	{
		throw ProcessWaitError();
	}

	if(WIFEXITED(ret))
	{
		return WEXITSTATUS(ret);
	}
	else
	{
		return boost::optional<int>();
	}
}

} /* namespace Core */
} /* namespace Neon */
