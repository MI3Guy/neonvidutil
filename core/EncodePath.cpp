/*
 * EncodePath.cpp
 *
 *  Created on: Mar 29, 2014
 *      Author: john
 */

#include "EncodePath.h"
#include "core/FileInputStream.h"

#include <map>
#include <stack>
#include <stdexcept>
#include <thread>

namespace Neon {
namespace Core {

EncodePath::EncodePath(NeonContext& ctx_, FormatType input, FormatType output)
	: ctx(ctx_),
	  steps(FindConversionPath(ctx_, input, output))
{
}

EncodePath::~EncodePath() {
}

std::vector<std::shared_ptr<FormatCodec>> EncodePath::FindConversionPath(NeonContext& ctx, FormatType input, FormatType output)
{
	std::map<FormatType, EncodeNode> graph;
	graph[input] = EncodeNode { };

	bool hasUpdated = true;

	while(hasUpdated)
	{
		hasUpdated = false;

		auto graphCopy = graph;
		for(const auto& graphNode : graphCopy)
		{
			for(ConversionInfo conversion : ctx.FindConversionTypes(graphNode.first))
			{
				int cost = graphNode.second.Cost + 1;
				try
				{
					if(graph.at(conversion.GetResult()).Cost <= cost)
					{
						continue;
					}
				}
				catch(std::out_of_range&)
				{
				}

				graph[conversion.GetResult()] = EncodeNode
				{
					cost,
					graphNode.first,
					conversion
				};
				hasUpdated = true;
			}
		}
	}

	if(graph.find(output) != graph.end())
	{
		std::vector<std::shared_ptr<FormatCodec>> path;

		FormatType next;
		FormatType curr = output;

		while(graph[curr].Previous.GetContainer() != FormatType::Container::None)
		{
			path.push_back(graph[curr].Conversion->CreateCodec());
			next = curr;
			curr = graph[curr].Previous;
		}

		return std::vector<std::shared_ptr<FormatCodec>>(path.rbegin(), path.rend());
	}
	else
	{
		throw NoEncodePathFound();
	}
}

void EncodePath::RunConversion(const std::string& inputFile, const std::string& outputFile)
{
	std::shared_ptr<InputStream> previous = [=]
	{
		auto pluginFile = ctx.OpenInput(inputFile);
		if(pluginFile)
		{
			return pluginFile;
		}
		else
		{
			return std::shared_ptr<InputStream>(new FileInputStream(inputFile));
		}
	}();

	struct EncodeStep
	{
		std::shared_ptr<FormatCodec> codec;
		std::shared_ptr<InputStream> inputStream;
		std::shared_ptr<OutputStream> outputStream;
		std::size_t progressId;

		void Run()
		{
			try
			{
				codec->ConvertData(IOStreamPair(inputStream, outputStream));
			}
			catch(std::exception& ex)
			{
				std::cerr << "An error of type " << typeid(ex).name() << " occurred. " << ex.what() << std::endl;
			}
			catch(...)
			{
				std::cerr << "An error occurred.";
			}
		}
	};

	std::vector<std::thread> threads;

	for(std::size_t i = 0; i < steps.size(); ++i)
	{
		std::shared_ptr<InputStream> input = previous;
		std::shared_ptr<OutputStream> output;
		if(i + 1 == steps.size())
		{
			output = steps[i]->InitConvertData(previous, outputFile);
		}
		else
		{
			IOStreamPair pair = steps[i]->InitConvertData(previous);
			output = pair.Output;
			previous = pair.Input;
		}

		EncodeStep step
		{
			steps[i],
			input,
			output,
			i
		};

		threads.emplace_back([](EncodeStep step){ step.Run(); }, step);
	}

	for(std::thread& t : threads)
	{
		t.join();
	}
}

const std::vector<std::shared_ptr<FormatCodec>>& EncodePath::GetSteps() const
{
	return steps;
}

} /* namespace Core */
} /* namespace Neon */
