/*
 * TempFileInputStream.cpp
 *
 *  Created on: Apr 17, 2014
 *      Author: john
 */

#include <core/TempFileInputStream.h>

#include <unistd.h>
#include <cstdio>

namespace Neon {
namespace Core {

TempFileInputStream::TempFileInputStream()
	: TempFileInputStream(CreateTempPath())
{

}

std::tuple<int, std::string> TempFileInputStream::CreateTempPath()
{
	char name[] = "/tmp/neonvidutil.XXXXXX";
	int fd = mkstemp(name);
	if(fd < -1)
	{
		throw FileOpenFailed();
	}
	return std::make_tuple(fd, name);
}

TempFileInputStream::TempFileInputStream(std::tuple<int, std::string> tempFile)
	: FileInputStream(std::get<0>(tempFile)),
	  path(std::get<1>(tempFile))
{
}

TempFileInputStream::~TempFileInputStream()
{
	if(fd != -1)
	{
		close(fd);
		fd = -1;
	}

	unlink(path.c_str());
}

std::string TempFileInputStream::GetPath() const
{
	return path;
}

} /* namespace Core */
} /* namespace Neon */
