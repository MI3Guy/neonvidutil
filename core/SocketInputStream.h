/*
 * SocketInputStream.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#pragma once

#include "core/InputStream.h"

#include <exception>
#include <string>
#include <string.h>
#include <thread>

namespace Neon {
namespace Core {

class SocketInputStream final : public InputStream {
public:
	SocketInputStream();
	virtual ~SocketInputStream();

private:
#ifdef _WIN32
#else
	int socketfd;
	int connfd;
#endif

	std::uint16_t port;
	std::thread connectWaitThread;

public:

	virtual std::size_t Read(void* buffer, std::size_t size) override;
	virtual bool CanSeek() const noexcept override;
	virtual LengthType Seek(LengthType pos) override;
	virtual LengthType Seek(SeekType type, LengthType pos) override;
	virtual LengthType Length() override;
	virtual LengthType Position() override;
	virtual void Close() override;

	std::uint16_t GetPort() const;

};

} /* namespace Core */
} /* namespace Neon */
