/*
 * PipeInputStream.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#pragma once

#include "core/InputStream.h"
#include "core/OutputStream.h"

#include <exception>
#include <unistd.h>
#include <string.h>
#include <memory>

namespace Neon {
namespace Core {

class PipeStream {

public:
	PipeStream();

protected:
	PipeStream(int); // Used to allow subclass to specify custom fds.

public:
	virtual ~PipeStream();

	std::shared_ptr<InputStream> CreateInputStream();
	std::shared_ptr<OutputStream> CreateOutputStream();

protected:
	class PipeInputStream final : public InputStream {
	public:
#ifdef _WIN32
#else
		PipeInputStream(int fd_);
#endif
		virtual ~PipeInputStream();

		virtual std::size_t Read(void* buffer, std::size_t size) override;
		virtual bool CanSeek() const noexcept override;
		virtual LengthType Seek(LengthType pos) override;
		virtual LengthType Seek(SeekType type, LengthType pos) override;
		virtual LengthType Length() override;
		virtual LengthType Position() override;
		virtual void Close() override;

	private:
#ifdef _WIN32
#else
	int fd;
#endif
	};

	class PipeOutputStream : public OutputStream
	{
	public:
#ifdef _WIN32
#else
		PipeOutputStream(int fd_);
#endif
		virtual ~PipeOutputStream();

		virtual void Write(const void* buffer, std::size_t size) override;
		virtual bool CanSeek() const noexcept override;
		virtual LengthType Seek(LengthType pos) override;
		virtual LengthType Seek(SeekType type, LengthType pos) override;
		virtual LengthType Length() override;
		virtual LengthType Position() override;
		virtual void Close() override;

	private:
	#ifdef _WIN32
	#else
		int fd;
	#endif
	};

	int readHandle;
	int writeHandle;


};

} /* namespace Core */
} /* namespace Neon */
