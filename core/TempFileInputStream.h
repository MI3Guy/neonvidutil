/*
 * TempFileInputStream.h
 *
 *  Created on: Apr 17, 2014
 *      Author: john
 */

#include "FileInputStream.h"

#pragma once

namespace Neon {
namespace Core {

class TempFileInputStream final : public FileInputStream
{
public:
	TempFileInputStream();

private:
	static std::tuple<int, std::string> CreateTempPath();
	TempFileInputStream(std::tuple<int, std::string> tempFile);

	std::string path;

public:
	virtual ~TempFileInputStream();

	std::string GetPath() const;
};

} /* namespace Core */
} /* namespace Neon */
