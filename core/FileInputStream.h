/*
 * FileInputStream.h
 *
 *  Created on: Mar 22, 2014
 *      Author: john
 */

#ifndef FILEINPUTSTREAM_H_
#define FILEINPUTSTREAM_H_

#include "core/InputStream.h"

#include <exception>
#include <string>
#include <string.h>

#ifdef _WIN32
#include <windows.h>
#endif

namespace Neon {
namespace Core {

class FileInputStream: public InputStream {
public:
	FileInputStream(const std::string& name);
protected:
	FileInputStream(int fd_);
public:
	virtual ~FileInputStream();

	virtual std::size_t Read(void* buffer, std::size_t size) override;
	virtual bool CanSeek() const noexcept override;
	virtual LengthType Seek(LengthType pos) override;
	virtual LengthType Seek(SeekType type, LengthType pos) override;
	virtual void SeekForward(std::size_t num) override;
	virtual LengthType Length() override;
	virtual LengthType Position() override;
	virtual void Close() override;

protected:
#ifdef _WIN32
	HANDLE fh;
#else
	int fd;
#endif
};

} /* namespace Core */
} /* namespace Neon */

#endif /* FILEINPUTSTREAM_H_ */
